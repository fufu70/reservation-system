<?php

class UserWidget extends CInputWidget
{
	public $list;
	
	/**
     * Initializes the widget.
     */
    public function init()
    {
        $data = User::model()->currentBranch()->unarchived()->findAll();
        $this->list = CHtml::listData($data, 'user_id', 'email_address');
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        $this->htmlOptions['class'] = 'chosen';
        $this->htmlOptions['empty'] = '';

        if ($this->hasModel())
        {
            echo CHtml::activeDropDownList($this->model, $this->attribute, $this->list, $this->htmlOptions);
        }
        else
        {
            echo CHtml::dropDownList($name, $this->value, $this->list, $this->htmlOptions);
        }
    }
}
