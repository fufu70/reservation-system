<?php

class ItemStatusWidget extends CInputWidget
{
	public $list;
	
	/**
     * Initializes the widget.
     */
    public function init()
    {
        $data = ItemStatus::model()->currentBranch()->unarchived()->findAll();
        $this->list = CHtml::listData($data, 'item_status_id', 'item_status_name');
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        $this->htmlOptions['class'] = 'chosen';
        $this->htmlOptions['empty'] = '';

        if ($this->hasModel())
        {
            echo CHtml::activeDropDownList($this->model, $this->attribute, $this->list, $this->htmlOptions);
        }
        else
        {
            echo CHtml::dropDownList($name, $this->value, $this->list, $this->htmlOptions);
        }
    }
}
