<?php

class TextWidget extends CInputWidget
{
	public $prepend = '';
    public $append = '';

    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        $hasPrepend = !empty($this->prepend);
        $hasAppend = !empty($this->append);

        if ($hasPrepend || $hasAppend)
            echo '<div class="input-group">';

        if ($hasPrepend)
            echo '<span class="input-group-addon">' . $this->prepend . '</span>';

        if ($this->hasModel())
        {
            echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
        }
        else
        {
            echo CHtml::textField($name, $this->value, $this->htmlOptions);
        }

        if ($hasAppend)
            echo '<span class="input-group-addon">' . $this->append . '</span>';

        if ($hasPrepend || $hasAppend)
            echo '</div>';
    }
}
