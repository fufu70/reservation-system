<?php

class FieldTypeWidget extends CInputWidget
{
	public $entityType;
	public $list;
	
	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		$data = FieldType::model()->currentBranch()->entityType($this->entityType)->findAll();
		$this->list = CHtml::listData($data, 'field_type_id', 'field_type_name');
	}

	/**
	 * Renders the widget.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameID();

		$this->htmlOptions['class'] = 'chosen';
		$this->htmlOptions['empty'] = '';

		if ($this->hasModel())
		{
			echo CHtml::activeDropDownList($this->model, $this->attribute, $this->list, $this->htmlOptions);
		}
		else
		{
			echo CHtml::dropDownList($name, $this->value, $this->list, $this->htmlOptions);
		}
	}
}
