<?php

class UserRoleWidget extends CInputWidget
{
	public $totalList;
    public $lowerList;
	
	/**
     * Initializes the widget.
     */
    public function init()
    {
        $totalUserRoles = UserRole::model()
            ->currentBranch()
            ->findAll();

        $lowerUserRoles = UserRole::model()
            ->currentBranch()
            ->maxRank(Yii::app()->user->role)
            ->findAll();

        $this->totalList = CHtml::listData($totalUserRoles, 'user_role_name', 'user_role_name');
        $this->lowerList = CHtml::listData($lowerUserRoles, 'user_role_name', 'user_role_name');
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        $this->htmlOptions['class'] = 'chosen';
        $this->htmlOptions['empty'] = '';
        
        $userRole = $this->model->getAttribute($this->attribute);

        if (isset($userRole) && !in_array($userRole, $this->lowerList))
        {
            $this->htmlOptions['disabled'] = 'true';
            $list = $this->totalList;
        }
        else
        {
            $list = $this->lowerList;
        }

        if ($this->hasModel())
        {
            echo CHtml::activeDropDownList($this->model, $this->attribute, $list, $this->htmlOptions);
        }
        else
        {
            echo CHtml::dropDownList($name, $this->value, $list, $this->htmlOptions);
        }
    }
}
