<?php

class ItemWidget extends CInputWidget
{
	public $list;
	public $types;
	public $multiple = false;
	
	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		$items = Item::model()->with('itemType')->currentBranch()->unarchived()->available()->findAll();
		$this->list = CHtml::listData($items, 'item_id', 'description', 'itemType.item_type_name');

		$itemTypes = ItemType::model()->currentBranch()->unarchived()->findAll();
		$this->types = array('All' => 'All');
		$this->types = array_merge($this->types, CHtml::listData($itemTypes, 'item_type_name', 'item_type_name'));
	}

	/**
	 * Renders the widget.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameID();

		$this->registerClientScript($id);

		if ($this->multiple == true)
			$this->htmlOptions['multiple'] = 'true';

		$this->htmlOptions['class'] = 'chosen';
		$this->htmlOptions['empty'] = '';

		echo '<div class="row">';
		echo '<div class="col-sm-6">';

		if ($this->hasModel())
		{
			echo CHtml::activeListBox($this->model, $this->attribute, $this->list, $this->htmlOptions);
		}
		else
		{
			echo CHtml::listBox($name, $this->value, $this->list, $this->htmlOptions);
		}

		echo '</div>';
		echo '<div class="col-sm-6">';

		echo '<div class="input-group">';
        echo '<span class="input-group-addon"><span class="glyphicon glyphicon-filter"></span></span>';

		echo CHtml::dropDownList($id . '_filter', 'All', $this->types, array('class' => 'chosen'));

		echo '</div>';
		echo '</div>';
		echo '</div>';
	}

	/**
	 * Registers the script for the widget.
	 */
	public function registerClientScript($id)
	{
		// Insert field ID into the javascript.
		$js = "$('[id={$id}_filter]').change(function() {
			var value = $(this).val();
			$('[id={$id}]').children('optgroup').each(function(index) {
				var state = (value != 'All') && (value != $(this).attr('label'));
				$(this).prop('disabled', state);
			});
			$('[id={$id}]').trigger('chosen:updated');
		});";

		$cs = Yii::app()->getClientScript();
		$cs->registerScript('Yii.ItemField#' . $id, $js);
	}
}
