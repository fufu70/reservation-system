<?php

Yii::import('application.components._input.DateTimeWidget');

class TimeWidget extends DateTimeWidget
{
    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->format = 'hh:mm A';
        $this->icon = 'time';

        parent::run();
    }
}
