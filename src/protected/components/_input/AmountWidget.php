<?php

class AmountWidget extends CInputWidget
{
    public $max = '99999.99';
    public $step = '0.01';
    public $decimals = 2;

    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        $this->registerClientScript($id);

        if ($this->hasModel())
        {
            echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
        }
        else
        {
            echo CHtml::textField($name, $this->value, $this->htmlOptions);
        }
    }

    /**
     * Registers the script for the widget.
     */
    public function registerClientScript($id)
    {
        $options = json_encode(array(
            'max' => $this->max,
            'step' => $this->step,
            'decimals' => $this->decimals,
            'buttonup_txt' => '<span class="glyphicon glyphicon-plus"></span>',
            'buttondown_txt' => '<span class="glyphicon glyphicon-minus"></span>',
        ));

        // Insert field ID and options into the javascript.
        $js = "var options = {$options};
            options.verticalbuttons = $(window).width() > 480;
            $('[id={$id}]').TouchSpin(options);";

        // Only show the spin buttons on hover.
        $js .= "$('[id={$id}]').siblings('.input-group-btn-vertical').hide();";
        $js .= "$('[id={$id}]').parent().removeClass('input-group');";
        $js .= "$('[id={$id}]').parent().hover(function() { 
            $(this).addClass('input-group').find('.input-group-btn-vertical').show();
        }, function() {
            $(this).removeClass('input-group').find('.input-group-btn-vertical').hide();
        });";

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-touchspin.min.js');
        $cs->registerScript('Yii.AmountField#' . $id, $js);
    }
}
