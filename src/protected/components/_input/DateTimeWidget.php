<?php

class DateTimeWidget extends CInputWidget
{
	public $format = 'YYYY-MM-DD hh:mm A';
	public $icon = 'calendar';

	/**
	 * Renders the widget.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameID();

		$this->registerClientScript($id);

		echo "<div class='input-group datepicker' id='{$id}_picker'>";
		echo '<span class="input-group-btn hidden-xs">';

		$label = '<span class="glyphicon glyphicon-' . $this->icon . '"></span>';

		echo CHtml::tag('button', array(
			'class' => 'btn btn-default dropdown-toggle',
			'type' => 'button',
		), $label);

		echo '</span>';

		if ($this->hasModel())
		{
			echo CHtml::activeDateTimeField($this->model, $this->attribute, $this->htmlOptions);
		}
		else
		{
			echo CHtml::dateTimeField($name, $this->value, $this->htmlOptions);
		}
		
		echo '</div>';
	}

	/**
     * Registers the script for the widget.
     */
    public function registerClientScript($id)
    {
    	$options = json_encode(array(
    		'format' => $this->format,
    	));

    	// Insert field ID and options into the javascript.
        $js = "$('[id={$id}_picker]').datetimepicker({$options});";

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/moment.min.js');
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-datetimepicker.min.js');
        $cs->registerScript('Yii.DateTimeField#' . $id, $js);
    }
}
