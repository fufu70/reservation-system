<?php

Yii::import('application.components._input.DateTimeWidget');

class DateWidget extends DateTimeWidget
{
	/**
	 * Renders the widget.
	 */
	public function run()
	{
		$this->format = 'YYYY-MM-DD';
		$this->icon = 'calendar';

		parent::run();
	}
}
