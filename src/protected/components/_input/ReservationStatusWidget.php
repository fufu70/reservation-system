<?php

class ReservationStatusWidget extends CInputWidget
{
	public $list = array(
        ReservationStatus::PENDING  => ReservationStatus::PENDING,
        ReservationStatus::READY    => ReservationStatus::READY,
        ReservationStatus::ACTIVE   => ReservationStatus::ACTIVE,
        ReservationStatus::OVERDUE  => ReservationStatus::OVERDUE,
        ReservationStatus::CLOSED   => ReservationStatus::CLOSED,
        ReservationStatus::CANCELED => ReservationStatus::CANCELED,
    );

    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        $this->htmlOptions['class'] = 'chosen';
        $this->htmlOptions['empty'] = '';

        if ($this->hasModel())
        {
            echo CHtml::activeDropDownList($this->model, $this->attribute, $this->list, $this->htmlOptions);
        }
        else
        {
            echo CHtml::dropDownList($name, $this->value, $this->list, $this->htmlOptions);
        }
    }
}
