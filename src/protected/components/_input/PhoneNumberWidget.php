<?php

class PhoneNumberWidget extends CMaskedTextField
{
	public $mask = '(999) 999-9999';
	public $class = 'phone';

	/**
	 * Renders the widget.
	 */
	public function run()
	{
		$this->htmlOptions['class'] = $this->class;

		echo '<div class="input-group">';
        echo '<span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>';
		
		parent::run();

		echo '</div>';
	}

	/**
	 * Registers the script for the widget.
	 */
	public function registerClientScript()
	{
		$js = "$(\".{$this->class}\").mask(\"{$this->mask}\");";

		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('maskedinput');
		$cs->registerScript('Yii.PhoneNumberField', $js);
	}  
}
