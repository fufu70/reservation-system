<?php

class ColorWidget extends CInputWidget
{
	public $colors = array(
		array('#EFEFEF', '#E79C9C', '#FFC69C', '#FFE79C', '#B5D6A5', '#A5C6CE', '#9CC6EF', '#B5A5D6', '#D6A5BD'),
		array('#CECECE', '#E76363', '#F7AD6B', '#FFD663', '#94BD7B', '#73A5AD', '#6BADDE', '#8C7BC6', '#C67BA5'),
		array('#9C9C9C', '#CE0000', '#E79439', '#EFC631', '#6BA54A', '#4A7B8C', '#3984C6', '#634AA5', '#A54A7B'),
		array('#636363', '#9C0000', '#B56308', '#BD9400', '#397B21', '#104A5A', '#085294', '#311873', '#731842'),
		array('#424242', '#630000', '#7B3900', '#846300', '#295218', '#083139', '#003163', '#21104A', '#4A1031'),
	);

	/**
	 * Renders the widget.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameID();

		$this->registerClientScript($id);

		echo '<div class="input-group">';
		echo '<div class="input-group-btn">';

		$label = '<span class="glyphicon glyphicon-tint"></span>';

		echo CHtml::tag('button', array(
			'class' => 'btn btn-default dropdown-toggle',
			'type' => 'button',
			'data-toggle' => 'dropdown',
		), $label);

		echo "<ul class='dropdown-menu'><li><div id='{$id}_colorPalette'></div></li></ul>";
		echo '</div>';

		$value = ($this->hasModel()) ? $this->model->{$this->attribute} : $this->value;

		echo CHtml::openTag('input', array(
			'type' => 'color',
			'style' => 'background-color: white; cursor: initial;',
			'disabled' => true,
			'value' => $value,
		));

		if ($this->hasModel())
		{
			echo CHtml::activeHiddenField($this->model, $this->attribute, $this->htmlOptions);
		}
		else
		{
			echo CHtml::hiddenField($name, $this->value, $this->htmlOptions);
		}
		
		echo '</div>';
	}

	/**
	 * Registers the script for the widget.
	 */
	public function registerClientScript($id)
	{
		$options = json_encode(array('colors' => $this->colors));

		// Insert field ID and options into the javascript.
		$js = "$('[id={$id}_colorPalette]').colorPalette({$options}).on('selectColor', function(e) {
			$(this).closest('.input-group').find('input').val(e.color);
		});";

		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-colorpalette.js');
		$cs->registerCssFile(Yii::app()->baseUrl . '/css/bootstrap-colorpalette.css');
		$cs->registerScript('Yii.ColorField#' . $id, $js);
	}
}
