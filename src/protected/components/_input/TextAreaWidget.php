<?php

class TextAreaWidget extends CInputWidget
{
    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        if ($this->hasModel())
        {
            echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
        }
        else
        {
            echo CHtml::textArea($name, $this->value, $this->htmlOptions);
        }
    }
}
