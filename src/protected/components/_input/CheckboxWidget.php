<?php

class CheckboxWidget extends CInputWidget
{
    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        $this->registerClientScript();

        $this->htmlOptions['class'] = 'switcher';

        echo '<div class="block">';

        if ($this->hasModel())
        {
            echo CHtml::activeCheckBox($this->model, $this->attribute, $this->htmlOptions);
        }
        else
        {
            echo CHtml::checkBox($name, $this->value, $this->htmlOptions);
        }
        
        echo '</div>';
    }

    /**
     * Registers the script for the widget.
     */
    public function registerClientScript()
    {
        $js = "$('.switcher').bootstrapSwitch({
            size: 'normal',
            onText: 'Yes',
            offText: 'No',
        });";

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-switch.min.js');
        $cs->registerScript('Yii.CheckboxField', $js);
    }
}
