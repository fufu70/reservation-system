<?php

class ItemTypeWidget extends CInputWidget
{
	public $list;
	
	/**
     * Initializes the widget.
     */
    public function init()
    {
        $data = ItemType::model()->currentBranch()->unarchived()->findAll();
        $this->list = CHtml::listData($data, 'item_type_id', 'item_type_name');
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        $this->htmlOptions['class'] = 'chosen';
        $this->htmlOptions['empty'] = '';

        if ($this->hasModel())
        {
            echo CHtml::activeDropDownList($this->model, $this->attribute, $this->list, $this->htmlOptions);
        }
        else
        {
            echo CHtml::dropDownList($name, $this->value, $this->list, $this->htmlOptions);
        }
    }
}
