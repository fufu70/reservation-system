<?php

class EmailAddressWidget extends CInputWidget
{
    /**
     * Renders the widget.
     */
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        echo '<div class="input-group">';
        echo '<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>';

        if ($this->hasModel())
        {
            echo CHtml::activeEmailField($this->model, $this->attribute, $this->htmlOptions);
        }
        else
        {
            echo CHtml::emailField($name, $this->value, $this->htmlOptions);
        }

        echo '</div>';
    }
}
