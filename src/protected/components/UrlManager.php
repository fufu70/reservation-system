<?php

/**
 * The UrlManager class is used to parse urls with camel case into hyphenated urls.
 *
 * @author John Salis <jsalis@stetson.edu>
 */
class UrlManager extends CUrlManager
{ 
    public function createUrl($route, $params = array(), $ampersand = '&')
    {
        $route = lcfirst($route);
        $route = preg_replace_callback('/(?<![A-Z])[A-Z]/', function($matches) {
            return '-' . lcfirst($matches[0]);
        }, $route);
        return parent::createUrl($route, $params, $ampersand);
    }
 
    public function parseUrl($request)
    {
        $route = parent::parseUrl($request);
        return lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $route))));
    }

    protected function processRules()
    {
        foreach (System::model()->findAll() as $system)
        {
            $name = strtolower($system->system_name);
            $this->rules['<system:' . $name . '>'] = 'doorkeeper/login';
            $this->rules['http://<system:' . $name . '>.xeres.io'] = 'doorkeeper/login';
        }
        parent::processRules();
    }
}