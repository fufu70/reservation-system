<?php

/**
 * The AutoForm class is an extension of CForm that automatically adds form elements to the
 * configuration based on the input types of model attributes. This only represents the structure 
 * of a form, not its presentation.
 *
 * @author   John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class AutoForm extends CForm
{
	/**
	 * Constructor.
	 * 
	 * @param array   $config  The configuration for the form.
	 * @param CModel  $model   The model associated with the form.
	 */
	function __construct($config, $model)
	{
		$hints = $model->hints();

		foreach ($model->inputTypes() as $name => $type)
		{
			$hint = isset($hints[$name]) ? $hints[$name] : '';
			
			$config['elements'][$name] = array(
				'type' => $type,
				'hint' => $hint,
			);

			// Disable autocomplete for text fields.
			if ($type == 'text')
			{
				$config['elements'][$name]['autocomplete'] = 'off';
			}
		}

		parent::__construct(array_merge($this->getBaseConfig(), $config), $model);
	}

	/**
	 * Gets the configuration that is applied to all forms. These can be overridden by the 
	 * configuration passed into the constructor.
	 * 
	 * @return array
	 */
	public function getBaseConfig()
	{
		return array(
			'activeForm' => array(
				'class' => 'CActiveForm',
				'enableAjaxValidation' => true,
				'enableClientValidation' => true,
				'clientOptions' => array(
					'validateOnSubmit' => true,
					'validateOnChange' => true,
					'validateOnType' => false,
				),
			),
		);
	}
}
