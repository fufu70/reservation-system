<?php

/**
 * WebUser repesents a user who is interacting with the application, and may or may not be logged in.
 * It is the persistent state for the web application user and contains information about the current 
 * system, branch, user role, and access points. Depending on the Yii app configuration, state information
 * will be stored in either cookies or the session.
 *
 * @author   John Salis <jsalis@stetson.edu>
 * @since    v2.0.0
 */
class WebUser extends CWebUser
{
	/**
	 * Logs in a user and initializes the state variables.
	 * 
	 * @param  CUserIdentity  	$identity 	The authenticated user identity.
	 * @param  int 				$duration 	Number of seconds that the user can remain logged in.
	 */
	public function login($identity, $duration = 0)
	{
		parent::login($identity, $duration);

		$this->setState('role', '');
		$this->setState('branch', '');
		$this->setState('system_id', '');
		$this->setState('access_list', array());
	}

	/**
	 * Selects the current system of the user, and sets the user role.
	 * 
	 * @param  int      $systemID  The ID of the system to select.
	 * @return boolean             Whether the system was selected.
	 */
	public function selectSystem($systemID)
	{
		$userRole = UserRole::model()->system($systemID)->user($this->id)->find();

		if (isset($userRole))
		{
			$this->system_id = $systemID;
			$this->role = $userRole->user_role_name;

			return true;
		}
		else
			return false;
	}

	/**
	 * Selects the current branch of the user, and sets the user role.
	 * 
	 * @param  int      $branchID  The ID of the branch to select.
	 * @return boolean             Whether the branch was selected.
	 */
	public function selectBranch($branchID)
	{
		$branch = Branch::model()->with('system')->findByPK($branchID);

		if ($this->selectSystem($branch->system->system_id))
		{
			// Set the current branch
			$this->branch = $branch;

			// Set the user role based on the current branch
			$userRole = UserRole::model()->branch($branchID)->user($this->id)->find();
			$this->role = (isset($userRole)) ? $userRole->user_role_name : UserRole::BASIC;

			$this->refreshAccessList();

			return true;
		}
		else
			return false;
	}

	/**
	 * Clears the currently selected branch, and resets the user role to system level access.
	 */
	public function clearBranch()
	{
		$this->branch = '';
		$this->role = '';

		$this->selectSystem($this->system_id);
	}

	/**
	 * Returns a value indicating whether a branch is selected.
	 * 
	 * @return boolean
	 */
	public function isBranchSelected()
	{
		return !empty($this->branch);
	}

	/**
	 * Refreshes the access list stored in the session by fetching from the database.
	 */
	public function refreshAccessList()
	{
		$this->access_list = Access::model()->currentBranch()->userRole($this->role)->findAll();
	}

	/**
	 * Checks if the user has access to an action.
	 * 
	 * @param  string  $route 		The route or url for the action.
	 * @return boolean        		Whether the user has access to the action.
	 */
	public function hasAccess($route)
	{
		if (!$this->isGuest && !empty($this->access_list))
		{
			foreach ($this->access_list as $access)
			{
				$url = Yii::app()->controller->createUrl($access->route);

				if ($access->route == $route || $url == $route)
				{
					return true;
				}
			}
		}

		return false;
	}
}
