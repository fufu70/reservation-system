<?php

Yii::import('zii.widgets.CMenu');

class SidebarMenu extends CMenu
{
    public $title;

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->htmlOptions['class'] = 'nav nav-pills nav-stacked';

        echo '<div class="panel panel-default sidebar-offcanvas" role="navigation">';

        if (isset($this->title))
        {
            echo '<div class="panel-heading"><h3 class="panel-title">' . $this->title . '</h3></div>';
        }
        
        echo '<div class="panel-body">';

        parent::run();

        echo '</div>';
        echo '</div>';
    }
}
