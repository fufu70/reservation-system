<?php

Yii::import('zii.widgets.CMenu');

class DropdownMenu extends CMenu
{
	public static $menuCount = 0;

	public $label = '&nbsp;';
	public $iconClass = 'fa';
	public $parentID = 'accordion_menu';

	/**
     * Initializes the widget.
     */
	public function init()
	{
		self::$menuCount ++;

		for ($i = 0; $i < count($this->items); $i ++)
		{
			$this->items[$i]['active'] = false;
		}
	}

	/**
	 * Renders the widget.
	 */
	public function run()
	{
		$this->htmlOptions['class'] = 'sidebar-nav-submenu';

		echo '<li class="dropdown panel">';

		echo '<a data-toggle="collapse" data-parent="#' . $this->parentID . '" href="#collapse_' . self::$menuCount . '">';
		echo '<i class="' . $this->iconClass . '"></i>' . $this->label;
		echo '</a>';

		echo '<div id="collapse_' . self::$menuCount . '" class="collapse">';

		parent::run();

		echo '</div>';
		echo '</li>';
	}
}
