<?php

/**
 * UserIdentity represents the data needed to identity a user. It contains the authentication 
 * function that checks if the provided data can identity the user.
 *
 * @author   John Salis <jsalis@stetson.edu>
 * @since    v2.0.0
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates the user and retrieves the user ID.
	 * 
	 * @return boolean	Whether authentication succeeded.
	 */
	public function authenticate()
	{
		$user = User::model()->emailAddress($this->username)->find();

		if (isset($user) && Login::authenticate($user->user_id, $this->password))
		{
			$this->_id = $user->user_id;
			$this->username = $user->fullName;
			
			return true;
		}
		else
			return false;
	}

	/**
	 * Gets the ID of the user.
	 * 
	 * @return int
	 */
	public function getId()
	{
		return $this->_id;
	}
}
