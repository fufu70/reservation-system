<?php

Yii::import('application.models._forms.FieldTypeForm');

/**
 * [LocationController description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class LocationController extends Controller
{
	private $_form;

	/**
	 * [actionCreate description]
	 */
	public function actionCreate()
	{
		if (isset($_POST['Location']))
		{
			$this->_form = new Location;
			$this->_form->attributes = $_POST['Location'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
		}

		$this->actionDisplay();
	}

	/**
	 * [actionRemove description]
	 */
	public function actionRemove($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$model = Location::model()->findByPk($id);

			$name = CHtml::encode($model->location_name);
			$message = '<div class="text-center">Are you sure you want to delete <b>' . $name . '</b>?</div>';

			$config = array(
				'id' => 'remove_location_form',
				'title' => 'Remove Location',
				'action' => '/location/remove',
				'elements' => array(
					'location_id' => array(
						'type' => 'hidden',
					),
					$message,
				),
			);

			$form = new CForm($config, $model);
			
			$this->renderPartial('/global/_modal-verify', array(
				'form' => $form,
			), false, true);

			Yii::app()->end();
		}
		else if (isset($_POST['Location']))
		{
			$id = $_POST['Location']['location_id'];

			$format = Yii::app()->params->dbDateFormat;
			$this->_form = Location::model()->findByPk($id);
			$this->_form->archived_date = (new DateTime)->format($format);
			$this->_form->save();
		}
		
		$this->actionDisplay();
	}

	/**
	 * [actionUpdate description]
	 */
	public function actionUpdate($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$model = Location::model()->findByPk($id);

			$config = array(
				'id' => 'update_location_form',
				'title' => 'Update Location',
				'action' => '/location/update',
			);

			$form = new AutoForm($config, $model);
			
			$this->renderPartial('/global/_form-modal', array(
				'form' => $form,
			), false, true);
			
			Yii::app()->end();
		}
		else if (isset($_POST['Location']))
		{
			$id = $_POST['Location']['location_id'];
			
			$this->_form = Location::model()->findByPk($id);
			$this->_form->attributes = $_POST['Location'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
		}

		$this->actionDisplay();
	}

	/**
	 * [actionDisplay description]
	 */
	public function actionDisplay()
	{
		$success = (isset($this->_form)) ? !$this->_form->hasErrors() : false;

		$this->render('/manage/index', array(
			'model' => new Location,
			'rows' => Location::model()->currentBranch()->unarchived()->findAll(),
			'success' => $success,
		));
	}
}
