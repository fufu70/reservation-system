<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class Controller extends CController
{
	/**
	 * A list of actions that can be called without authentication.
	 * 
	 * @var array
	 */
	protected $publicActions = array();

	/**
	 * Checks if the user has access to the requested action. If so, the layout is initialized.
	 *
	 * @param  Action 	$action 	The requested action.
	 * @return boolean 				States whether the action should be executed.
	 */
	protected function beforeAction($action)
	{
		if (in_array($action->id, $this->publicActions))
		{
			return true;
		}
		else if (Yii::app()->user->isGuest)
		{
			Yii::app()->user->loginRequired();
			return false;
		}
		else if ($this->isBranchRequired())
		{
			$this->redirect(array('branch/select'));
		}
		else if (!$this->userHasAccess($action))
		{
			Yii::app()->user->refreshAccessList();
			throw new CHttpException(403, 'The requested action is forbidden.');
			return false;
		}
		else
		{
			$this->initializeLayout();
			return true;
		}
	}

	/**
	 * Checks if a branch needs to be selected before the action can be performed.
	 *
	 * @return boolean 	        	States whether a branch must be selected
	 */
	private function isBranchRequired()
	{
		return !Yii::app()->user->isBranchSelected() && 
			$this->route != 'branch/select' && 
			$this->route != 'branch/create';
	}

	/**
	 * Checks if the current user has access to the controller action.
	 *
	 * @param  Action 	$action 	The requested action.
	 * @return boolean 	        	States whether the user has access to the controller action.
	 */
	private function userHasAccess($action)
	{
		if ($action->controller->id == 'account' || $action->controller->id == 'branch') // TEMP
		{
			return true;
		}

		return Access::model()
			->userRole(Yii::app()->user->role)
			->controller($action->controller->id)
			->action($action->id)
			->exists();
	}

	/**
	 * Sets the layout for the controller.
	 */
	private function initializeLayout()
	{
		$this->layout = 'default';
	}

	/**
	 * Performs validation on the specified form if the request is ajax.
	 * 
	 * @param CFormModel 	$form 	The form to validate.
	 */
	protected function performAjaxValidation($form)
	{
	    if (Yii::app()->request->isAjaxRequest)
	    {
	        echo CActiveForm::validate(array($form));
	        Yii::app()->end();
	    }
	}

	/**
	 * Converts an array to a CSV file and sends it to the output buffer.
	 * 	
	 * @param  array   $array       The list of information to be converted to a CSV file.
	 * @param  string  $filename    The name of the CSV file.
	 * @param  string  $delimeter   The column delimeter for the CSV.
	 */
	public function arrayToCSV($array, $filename = 'export.csv', $delimeter = ',')
	{
		if (count($array) != 0)
		{
			// Opens a spot in memory and sets it to writable.
			$f = fopen('php://memory', 'w');

			fputcsv($f, array_keys($array[0]), $delimeter);

			foreach ($array as $line)
			{
				fputcsv($f, $line, $delimeter);
			}

			fseek($f, 0);
			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename="' . $filename . '"');

			// Places the CSV into the output buffer.
			fpassthru($f);
		}
	}
}
