<?php

Yii::import('application.models._forms.ChangePasswordForm');

/**
 * The AccountController provides actions for displaying and updating the current user's account 
 * information including name, email, password, as well as actions for payment and activation
 * of application modules.
 *
 * @author   John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class AccountController extends Controller
{
	private $_form;

	/**
	 * The root action redirects to the action for personal settings.
	 */
	public function actionIndex()
	{
		$this->redirect(array('personalSettings'));
	}

	/**
	 * Renders the form view for personal settings. Also updates the information for the current 
	 * user if the correct post is set.
	 */
	public function actionPersonalSettings()
	{
		if (isset($_POST['User']))
		{
			$this->_form = User::model()->findByPk(Yii::app()->user->id);
			$this->_form->attributes = $_POST['User'];
			$this->performAjaxValidation($this->_form);
			
			$success = $this->_form->save();

			Yii::app()->user->name = $this->_form->fullName;
		}

		$this->render('index', array(
			'formView' => '_personal-settings',
			'success' => !empty($success)
		));
	}

	/**
	 * Renders the form view for changing the account password. Also updates the password for the 
	 * current user if the correct post is set.
	 */
	public function actionChangePassword()
	{
		if (isset($_POST['ChangePasswordForm']))
		{
			$this->_form = new ChangePasswordForm;
			$this->_form->attributes = $_POST['ChangePasswordForm'];
			$this->performAjaxValidation($this->_form);

			$success = $this->_form->submit();
		}

		$this->render('index', array(
			'formView' => '_change-password',
			'success' => !empty($success)
		));
	}
}
