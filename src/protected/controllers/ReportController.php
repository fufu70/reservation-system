<?php

/**
 * The ReportController class is there to act as a middle man between all of the views in which the
 * user selects what he wants (reservations from a specific time, with a specific item, ...) and
 * then either allows the user to display all of that information or download it in a csv format.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0	
 */
class ReportController extends Controller
{
	protected $publicActions = array('fetchCriteria');

	/**
	 * [actionReservation description]
	 */
	public function actionReservation()
	{
		if (isset($_POST['Criteria']))
		{
			$model = Reservation::model()->currentBranch();

			Report::model()->buildQuery($model, $_POST['Criteria']);

			$this->render('results', array(
				'rows' => $model->findAll()
			));
		}
		else
		{
			$this->render('search-reservation');
		}
	}

	/**
	 * [actionItem description]
	 */
	public function actionItem()
	{
		if (isset($_POST['Criteria']))
		{
			$model = Item::model()->currentBranch();

			Report::model()->buildQuery($model, $_POST['Criteria']);

			$this->render('results', array(
				'rows' => $model->findAll()
			));
		}
		else
		{
			$this->render('search-item');
		}
	}

	/**
	 * AJAX-Only Request.
	 * 
	 * [fetchCriteria description]
	 */
	public function actionFetchCriteria()
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$label = ucwords(str_replace('_', ' ', $_GET['label']));
			
			$this->renderPartial('_criteria', array(
				'label' => $label,
				'attribute' => $_GET['attribute'],
				'inputType' => $_GET['inputType'],
				'comparable' => $_GET['comparable'],
			), false, true);

			Yii::app()->end();
		}
		else
		{
			throw new CHttpException('400', 'Invalid request.');
		}
	}
}
