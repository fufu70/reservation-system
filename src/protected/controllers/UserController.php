<?php

Yii::import('application.models._forms.FieldTypeForm');

/**
 * [UserController description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class UserController extends Controller
{
	private $_form;

	/**
	 * [actionCreate description]
	 */
	public function actionCreate()
	{
		if (isset($_POST['User']))
		{
			$this->_form = new User;
			$this->_form->attributes = $_POST['User'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
		}

		$this->actionDisplay();
	}

	/**
	 * [actionRemove description]
	 */
	public function actionRemove($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$model = User::model()->findByPk($id);

			$name = CHtml::encode($model->email_address);
			$message = '<div class="text-center">Are you sure you want to delete <b>' . $name . '</b>?</div>';

			$config = array(
				'id' => 'remove_user_form',
				'title' => 'Remove User',
				'action' => '/user/remove',
				'elements' => array(
					'user_id' => array(
						'type' => 'hidden',
					),
					$message,
				),
			);

			$form = new CForm($config, $model);
			
			$this->renderPartial('/global/_modal-verify', array(
				'form' => $form,
			), false, true);

			Yii::app()->end();
		}
		else if (isset($_POST['User']))
		{
			$id = $_POST['User']['user_id'];

			$format = Yii::app()->params->dbDateFormat;
			$this->_form = BranchHasUser::model()->branch(Yii::app()->user->branch->branch_id)->user($id)->find();
			$this->_form->archived_date = (new DateTime)->format($format);
			$this->_form->save();
		}
		
		$this->actionDisplay();
	}

	/**
	 * [actionUpdate description]
	 */
	public function actionUpdate($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$model = User::model()->findByPk($id);

			$config = array(
				'id' => 'update_user_form',
				'title' => 'Update User',
				'action' => '/user/update',
			);

			$form = new AutoForm($config, $model);
			
			$this->renderPartial('/global/_form-modal', array(
				'form' => $form,
			), false, true);

			Yii::app()->end();
		}
		else if (isset($_POST['User']))
		{
			$id = $_POST['User']['user_id'];

			$this->_form = User::model()->findByPk($id);
			$this->_form->attributes = $_POST['User'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
		}

		$this->actionDisplay();
	}

	/**
	 * [actionDisplay description]
	 */
	public function actionDisplay()
	{
		$success = (isset($this->_form)) ? !$this->_form->hasErrors() : false;

		$this->render('/manage/index', array(
			'model' => new User,
			'rows' => User::model()->unarchived()->currentBranch()->findAll(),
			'success' => $success,
		));
	}
}
