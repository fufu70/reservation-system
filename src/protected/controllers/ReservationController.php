<?php

/**
 * [ReservationController description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class ReservationController extends Controller
{
	protected $publicActions = array('fetchDetails', 'fetchReservations');

	private $_form;

	/**
	 * This action creates a new reservation for the current branch.
	 */
	public function actionCreate()
	{
		if (isset($_POST['Reservation']))
		{
			$this->_form = new Reservation;
			$this->_form->attributes = $_POST['Reservation'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
		}

		$this->actionDisplay();
	}

	/**
	 * This action updates the details of a reservation.
	 */
	public function actionUpdate($id = null)
	{
		if (isset($id))
		{
			$this->render('/reservation/index', array(
				'reservationID' => $id,
				'model' => new Reservation,
				'rows' => Reservation::model()->currentBranch()->inProgress()->findAll(),
			));
		}
		else if (isset($_POST['Reservation']))
		{
			$id = $_POST['Reservation']['reservation_id'];

			$this->_form = Reservation::model()->findByPk($id);
			$this->_form->attributes = $_POST['Reservation'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();

			$this->actionDisplay();
		}
		else
		{
			$this->actionDisplay();
		}
	}

	public function actionDisplay()
	{
		$this->render('/reservation/index', array(
			'model' => new Reservation,
			'rows' => Reservation::model()->currentBranch()->inProgress()->findAll(),
		));
	}

	/**
	 * This action checks out a reservation by setting the status to active.
	 */
	public function actionCheckout($id = null)
	{
		if (isset($id))
		{
			Reservation::model()->checkout($id);
		}

		$this->redirect(array('branch/dashboard/'));
	}

	/**
	 * This action closes a reservation by setting the status to closed.
	 */
	public function actionClose($id = null)
	{
		if (isset($id))
		{
			Reservation::model()->close($id);
		}

		$this->redirect(array('branch/dashboard/'));
	}

	/**
	 * This action cancels a reservation by setting the status to canceled.
	 */
	public function actionCancel($id = null)
	{
		if (isset($id))
		{
			Reservation::model()->cancel($id);
		}

		$this->redirect(array('branch/dashboard/'));
	}
	
	/**
	 * This action displays the reservation as a printable user agreement. 
	 * If a reservation ID is not set, then the action redirects to the branch index.
	 */
	public function actionAgreement($id = null)
	{
		if (isset($id))
		{
			$reservation = Reservation::model()->with('user', 'reservationHasItems.item')->findByPK($id);

			$this->render('agreement', array(
				'reservation' => $reservation,
			));
		}
		else
		{
			$this->redirect(array('branch/'));
		}
	}

	/**
	 * AJAX-Only Request.
	 * 
	 * Renders the details for a single reservation inside a modal.
	 */
	public function actionFetchDetails($id)
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$reservation = Reservation::model()->with('user', 'reservationHasItems.item')->findByPk($id);

			$this->renderPartial('_reservation-details', array(
				'reservation' => $reservation,
			), false, true);

			Yii::app()->end();
		}
		else
		{
			throw new CHttpException('400', 'Invalid request.');
		}
	}

	/**
	 * AJAX-Only Request.
	 * 
	 * Returns a JSON encoded array of calendar events for reservations between a start and end date.
	 */
	public function actionFetchReservations($start, $end)
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$colorSettings = Setting::model()
				->currentBranch()
				->type(SettingType::RESERVATION_STATUS_COLOR)
				->findAll();

			$resStatusColors = CHtml::listData($colorSettings, 'setting_attribute', 'setting_value');

			$reservations = Reservation::model()
				->with('user', 'reservationStatus')
				->currentBranch()
				->dateRange($start, $end)
				->findAll();

			$calEvents = array();

			foreach ($reservations as $reservation)
			{
				$status = $reservation->isOverdue() ? ReservationStatus::OVERDUE : $reservation->reservationStatus->reservation_status_name;

				// Create a DateTime object for the beginning and end date.
				$format = Yii::app()->params->dbDateFormat;
				$startDate = DateTime::createFromFormat($format, $reservation->beginning_date);
				$endDate = DateTime::createFromFormat($format, $reservation->end_date);

				$textColor = 'white';

				$calEvents[] = array(
					'id' => $reservation->reservation_id,
					'status' => $status,
					'title' => $reservation->user->fullName,
					'color' => $resStatusColors[ $status ],
					'textColor' => $textColor,
					'start' => $startDate->format('Y-m-d H:i'),
					'end' => $endDate->format('Y-m-d H:i'),
					'visible' => true,
				);
			}

			echo json_encode($calEvents);
		}
		else
		{
			throw new CHttpException('400', 'Invalid request.');
		}
	}
}
