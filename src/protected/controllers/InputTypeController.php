<?php

/**
 * [InputTypeController description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class InputTypeController extends Controller
{
	protected $publicActions = array('fetchInput');

	/**
	 * AJAX-Only Request.
	 * 
	 * [FetchInput description]
	 */
	public function actionFetchInput($inputType)
	{
		if (Yii::app()->request->isAjaxRequest)
		{
			$path = InputType::model()->getPathToWidget($inputType);
			$field = $this->widget($path, array('name' => 'preview'), true);

			echo $this->processOutput($field);

			Yii::app()->end();
		}
		else
		{
			throw new CHttpException('400', 'Invalid request.');
		}
	}
}