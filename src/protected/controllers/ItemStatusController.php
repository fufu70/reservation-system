<?php

Yii::import('application.models._forms.FieldTypeForm');

/**
 * [ItemStatusController description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class ItemStatusController extends Controller
{
	private $_form;

	/**
	 * [actionCreate description]
	 */
	public function actionCreate()
	{
		if (isset($_POST['ItemStatus']))
		{
			$this->_form = new ItemStatus;
			$this->_form->attributes = $_POST['ItemStatus'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
		}

		$this->actionDisplay();
	}

	/**
	 * [actionRemove description]
	 */
	public function actionRemove($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$model = ItemStatus::model()->findByPk($id);

			$name = CHtml::encode($model->item_status_name);
			$message = '<div class="text-center">Are you sure you want to delete <b>' . $name . '</b>?</div>';

			$config = array(
				'id' => 'remove_item_status_form',
				'title' => 'Remove Item Status',
				'action' => '/itemStatus/remove',
				'elements' => array(
					'item_status_id' => array(
						'type' => 'hidden',
					),
					$message,
				),
			);

			$form = new CForm($config, $model);
			
			$this->renderPartial('/global/_modal-verify', array(
				'form' => $form,
			), false, true);

			Yii::app()->end();
		}
		else if (isset($_POST['ItemStatus']))
		{
			$id = $_POST['ItemStatus']['item_status_id'];

			$format = Yii::app()->params->dbDateFormat;
			$this->_form = ItemStatus::model()->findByPk($id);
			$this->_form->archived_date = (new DateTime)->format($format);
			$this->_form->save();
		}
		
		$this->actionDisplay();
	}

	/**
	 * [actionUpdate description]
	 */
	public function actionUpdate($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$model = ItemStatus::model()->findByPk($id);

			$config = array(
				'id' => 'update_item_status_form',
				'title' => 'Update Item Status',
				'action' => '/itemStatus/update',
			);

			$form = new AutoForm($config, $model);
			
			$this->renderPartial('/global/_form-modal', array(
				'form' => $form,
			), false, true);
			
			Yii::app()->end();
		}
		else if (isset($_POST['ItemStatus']))
		{
			$id = $_POST['ItemStatus']['item_status_id'];

			$this->_form = ItemStatus::model()->findByPk($id);
			$this->_form->attributes = $_POST['ItemStatus'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
		}

		$this->actionDisplay();
	}

	/**
	 * [actionDisplay description]
	 */
	public function actionDisplay()
	{
		$success = (isset($this->_form)) ? !$this->_form->hasErrors() : false;

		$this->render('/manage/index', array(
			'model' => new ItemStatus,
			'rows' => ItemStatus::model()->currentBranch()->unarchived()->findAll(),
			'success' => $success,
		));
	}
}
