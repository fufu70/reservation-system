<?php

/**
 * The BranchController provides actions to select and create branches, and change branch settings
 * and access points.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class BranchController extends Controller
{
	/**
	 * Redirects to the dashboard.
	 */
	public function actionIndex()
	{
		$this->redirect(array('dashboard'));
	}

	/**
	 * Renders the dashboard view.
	 */
	public function actionDashboard()
	{
		$this->render('dashboard');
	}

	/**
	 * Selects the current branch and redirects to the index if the user has access, otherwise
	 * the branch list view is rendered.
	 * 
	 * @param int 	$id 	The ID of the branch to select.
	 */
	public function actionSelect($id = null)
	{
		$user = User::model()->findByPk(Yii::app()->user->id);

		if (isset($id) && Yii::app()->user->selectBranch($id))
		{
			$this->redirect(array('branch/'));
		}
		else
		{
			Yii::app()->user->clearBranch();
		}

		$this->layout = 'doorkeeper';
		$this->render('/doorkeeper/branch-list', array(
			'tab' => 'select_branch',
			'branchList' => $user->branches,
		));
	}

	/**
	 * [actionCreate description]
	 */
	public function actionCreate()
	{
		if (isset($_POST['Branch']))
		{
			$branch = new Branch;
			$branch->attributes = $_POST['Branch'];
			$branch->system_id = 1;
			$this->performAjaxValidation($branch);
			$branch->save();

			$this->redirect(array('select', 'id' => $branch->branch_id));
		}
		else
		{
			$this->redirect(array('index'));
		}
	}

	/**
	 * This action changes the access roles when the user has submitted the form, otherwise the 
	 * access view is rendered.
	 */
	public function actionAccess()
	{
		if (isset($_POST['Access']))
		{
			Access::model()->updateFromList($_POST['Access']);
		}

		$this->render('access');
	}

	/**
	 * [actionSettings description]
	 */
	public function actionSettings()
	{
		if (isset($_POST['Setting']))
		{
			foreach ($_POST['Setting'] as $id => $value)
			{
				if ($value != '')
					Setting::model()->updateByPk($id, array('setting_value' => $value));
			}
		}

		$this->render('setting', array(
			'settingList' => Setting::model()->currentBranch()->activated()->findAll(),
		));
	}
}
