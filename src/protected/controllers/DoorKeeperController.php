<?php

Yii::import('application.models._forms.DoorKeeperForm');

/**
 * The DoorKeeperController class controls log in and log out, and sets up session information 
 * using the DoorKeeperForm class.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class DoorKeeperController extends Controller
{
	public $layout = 'doorkeeper';

	private $_system;
	private $_form;

	public function beforeAction($action)
	{
		if ($action->id == 'logout' || $action->id == 'error')
		{
			return true;
		}
		else if (!Yii::app()->user->isGuest)
		{
			$this->redirect(array('branch/'));
			return false;
		}
		else if (isset($_GET['system']))
		{
			$name = strtolower($_GET['system']);
			$this->_system = System::model()->name($name)->find();
			return true;
		}
		else
		{
			return true;
		}
	}

	public function actionIndex()
	{
		$this->render('index', array(
			'system' => $this->_system,
		));
	}

	public function actionLogin()
	{
		$this->_form = new DoorKeeperForm('login');

		if (isset($_POST['DoorKeeper']))
		{
			$this->_form->attributes = $_POST['DoorKeeper'];

			if ($this->_form->submit())
			{
				if (isset($_POST['DoorKeeper']['system_id']))
				{
					Yii::app()->user->selectSystem($_POST['DoorKeeper']['system_id']);
				}
				
				$this->redirect(array('branch/select'));
			}
		}

		$this->render('index', array(
			'system' => $this->_system,
		    'model' => $this->_form,
		    'tab' => 'login',
		));
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('doorkeeper/'));
	}

	public function actionSignup()
	{
		$this->_form = new DoorKeeperForm('signup');

		if (isset($_POST['DoorKeeper']))
		{
			$this->_form->attributes = $_POST['DoorKeeper'];
			if ($this->_form->submit())
			{
				$this->actionLogin();
				return;
			}
		}

		$this->render('index', array(
			'system' => $this->_system,
			'model' => $this->_form,
			'tab' => 'signup',
		));
	}

	public function actionError()
	{
	    if ($error = Yii::app()->errorHandler->error)
	    {
	    	$this->render('error', $error);
	    }
	    else
	    {
	    	$this->redirect(array('doorkeeper/'));
	    }
	}
}
