<?php

/**
 * The SiteController provides actions for viewing information about the app, including features,
 * pricing, and contact information. Login is not required.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class SiteController extends CController
{
	public function actionIndex()
	{
		
	}
}
