<?php

Yii::import('application.models._forms.FieldTypeForm');

/**
 * [ItemTypeController description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class ItemTypeController extends Controller
{
	private $_form;

	/**
	 * [actionCreate description]
	 */
	public function actionCreate()
	{
		if (isset($_POST['ItemType']))
		{
			$this->_form = new ItemType;
			$this->_form->attributes = $_POST['ItemType'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
		}

		$this->actionDisplay();
	}

	/**
	 * [actionRemove description]
	 */
	public function actionRemove($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$model = ItemType::model()->findByPk($id);

			$name = CHtml::encode($model->item_type_name);
			$message = '<div class="text-center">Are you sure you want to delete <b>' . $name . '</b>?</div>';

			$config = array(
				'id' => 'remove_item_type_form',
				'title' => 'Remove Item Type',
				'action' => '/itemType/remove',
				'elements' => array(
					'item_type_id' => array(
						'type' => 'hidden',
					),
					$message,
				),
			);

			$form = new CForm($config, $model);
			
			$this->renderPartial('/global/_modal-verify', array(
				'form' => $form,
			), false, true);

			Yii::app()->end();
		}
		else if (isset($_POST['ItemType']))
		{
			$id = $_POST['ItemType']['item_type_id'];

			$format = Yii::app()->params->dbDateFormat;
			$this->_form = ItemType::model()->findByPk($id);
			$this->_form->archived_date = (new DateTime)->format($format);
			$this->_form->save();
		}
		
		$this->actionDisplay();
	}

	/**
	 * [actionUpdate description]
	 */
	public function actionUpdate($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$model = ItemType::model()->findByPk($id);

			$config = array(
				'id' => 'update_item_type_form',
				'title' => 'Update Item Type',
				'action' => '/itemType/update',
			);

			$form = new AutoForm($config, $model);
			
			$this->renderPartial('/global/_form-modal', array(
				'form' => $form,
			), false, true);

			Yii::app()->end();
		}
		else if (isset($_POST['ItemType']))
		{
			$id = $_POST['ItemType']['item_type_id'];

			$this->_form = ItemType::model()->findByPk($id);
			$this->_form->attributes = $_POST['ItemType'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
		}

		$this->actionDisplay();
	}

	/**
	 * [actionDisplay description]
	 */
	public function actionDisplay()
	{
		$success = (isset($this->_form)) ? !$this->_form->hasErrors() : false;

		$this->render('/manage/index', array(
			'model' => new ItemType,
			'rows' => ItemType::model()->currentBranch()->unarchived()->findAll(),
			'success' => $success,
		));
	}
}
