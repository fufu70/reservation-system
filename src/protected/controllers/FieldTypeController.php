<?php

/**
 * [FieldTypeController description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class FieldTypeController extends Controller
{
	private $_form;

	/**
	 * If the correct POST is set, this action creates a new field type then renders the manager view 
	 * for the associated entity. Otherwise this action redirects to the site index. 
	 */
	public function actionCreate()
	{
		if (isset($_POST['FieldType']))
		{
			$this->_form = new FieldType;
			$this->_form->attributes = $_POST['FieldType'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();
			
			$this->displayEntity();
		}

		$this->redirect(array('branch/'));
	}

	/**
	 * [actionRemove description]
	 */
	public function actionRemove()
	{
		if (isset($_POST['FieldType']))
		{
			// TO DO : Implement remove.
			
			$this->displayEntity();
		}
		
		$this->redirect(array('branch/'));
	}

	/**
	 * [actionUpdate description]
	 */
	public function actionUpdate($id = null)
	{
		if (isset($id) && Yii::app()->request->isAjaxRequest)
		{
			$model = FieldType::model()->findByPk($id);

			echo json_encode($model->attributes);
			
			Yii::app()->end();
		}
		else if (isset($_POST['FieldType']))
		{
			$id = $_POST['FieldType']['field_type_id'];

			$this->_form = FieldType::model()->findByPk($id);
			$this->_form->attributes = $_POST['FieldType'];
			$this->performAjaxValidation($this->_form);
			$this->_form->save();

			$this->displayEntity();
		}

		$this->redirect(array('branch/'));
	}

	/**
	 * [displayEntity description]
	 */
	private function displayEntity()
	{
		if (isset($this->_form->entityType->entity_type_name))
		{
			$entityType = str_replace(' ', '', $this->_form->entityType->entity_type_name);
			$this->redirect(array($entityType . '/display'));
		}
	}
}
