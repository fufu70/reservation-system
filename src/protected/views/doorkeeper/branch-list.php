<?php
    $tab = (isset($tab)) ? $tab : 'select_branch';
    $branchList = (isset($branchList)) ? $branchList : array();
?>

<div class="panel panel-body">
    <!-- Nav List -->
    <ul class="tab-list">
        <li class="<?php if ($tab == 'select_branch') echo 'active'; ?>">
            <a href="#select_branch" data-toggle="tab">Select Branch</a>
        </li>
        <li class="<?php 
            if (Yii::app()->user->role != UserRole::SUPER) echo 'hide';
            else if ($tab == 'create_branch') echo 'active'; 
            ?>">
            <a href="#create_branch" data-toggle="tab">Create Branch</a>
        </li>
    </ul>
    <br />

    <!-- Tab content -->
    <div class="tab-content">

        <div class="tab-pane fade <?php if ($tab == 'select_branch') echo 'in active'; ?>" id="select_branch">
            <div class="list-group scrollable">
                <?php
                    foreach ($branchList as $branch)
                    {
                        echo '<a href="/branch/select/id/' . $branch->branch_id . '" class="list-group-item';
                        if (Yii::app()->user->branch != '' && Yii::app()->user->branch->branch_id == $branch->branch_id)
                        {
                            echo ' active';
                        }
                        echo '">';
                        echo '<p class="list-group-item-heading lead">' . $branch->branch_name . '</p>';
                        echo '<p class="list-group-item-text">' . $branch->description . '</p>';
                        echo '</a>';
                    }
                ?>
            </div>
            <div class="form-signin alert alert-warning <?php if (sizeof($branchList) > 0) echo 'hide'; ?>">
                No available branches found.
                <?php
                    if (Yii::app()->user->role == UserRole::SUPER)
                    {
                        echo ' Create a branch to get started.';
                    }
                ?>
            </div>
            <a href="/doorkeeper/logout" class="btn btn-default btn-block">Log Out</a>
        </div> <!-- /select_branch -->

        <div class="tab-pane fade <?php if ($tab == 'create_branch') echo 'in active'; ?>" id="create_branch">
            <form action="/branch/create" method="post">
                <div class="form-group">
                    <input name="Branch[branch_name]" type="text" class="form-control" placeholder="Branch Name" required>
                    <textarea rows="3" style="resize: none;" name="Branch[description]" type="text" class="form-control" placeholder="Branch Description" required></textarea>
                </div>
                <button class="btn btn-primary btn-block" type="submit">Create Branch</button>
            </form> 
        </div> <!-- /create_branch -->

    </div> <!-- /tab-content -->
</div>
