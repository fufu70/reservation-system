<?php
	$this->pageTitle = Yii::app()->name . ' - Error';
?>

<div class="panel panel-body">
	<span class="text-center">
		<h2>Error <?php echo $code; ?></h2>

		<div class="form-group">
			<img src="https://stampics.com/wp-content/uploads/2014/04/meme_pff-300x300.jpg">
		</div>

		<div class="error">
			<br>
			<?php echo CHtml::encode($message); ?>
		</div>
	<span>
</div>
