<?php
    $tab = (isset($tab)) ? $tab : 'login';
    $systemID = (isset($system)) ? $system->system_id : '';
    $errorMessage = '';

    if (isset($model))
    {
        foreach ($model->getErrors() as $key => $value)
        {
            $errorMessage .= $model->getError($key) . '<br>';
        }
    }
?>

<h1 style="font-family: Ubuntu"><?php echo Yii::app()->name; ?><br><small>Reservation Engine</small></h1>

<div class="panel panel-body">
    <!-- Nav tabs -->
    <ul class="tab-list">
        <li class="<?php if ($tab == 'login') echo 'active'; ?>"><a href="#login" data-toggle="tab">Log In</a></li>
        <li class="<?php if ($tab == 'signup') echo 'active'; ?>"><a href="#signup" data-toggle="tab">Create Account</a></li>
    </ul><br>

    <!-- Tab content -->
    <div class="tab-content">

        <div class="tab-pane fade <?php if ($tab == 'login') echo 'in active'; ?>" id="login">
            <form action="/doorkeeper/login" method="post">
                <?php
                    if (isset($system))
                    {
                        echo '<h3>' . $system->system_name . ' login</h3>';
                    }
                ?>
                <input name="DoorKeeper[system_id]" type="hidden" value="<?php echo $systemID; ?>" required>
                <div class="form-group">
                    <input name="DoorKeeper[email_address]" type="text" class="form-control" placeholder="Email" autofocus required>
                    <input name="DoorKeeper[password]" type="password" class="form-control" placeholder="Password" required>
                </div>
                <div class="form-group pull-right">
                    <label for="DoorKeeper_rememberMe">Remember me?</label>
                    <?php $this->widget(InputType::model()->getPathToWidget(InputType::CHECKBOX), array('name' => 'DoorKeeper[rememberMe]')); ?>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button>
            </form>
        </div> <!-- /login -->

        <div class="tab-pane fade <?php if ($tab == 'signup') echo 'in active'; ?>" id="signup">
            <form action="/doorkeeper/signup" method="post">
                <div class="form-group">
                    <input name="DoorKeeper[first_name]" type="text" class="form-control" placeholder="First Name" required>
                    <input name="DoorKeeper[last_name]" type="text" class="form-control" placeholder="Last Name" required>
                    <input name="DoorKeeper[email_address]" type="text" class="form-control" placeholder="Email" required>
                    <input name="DoorKeeper[password]" type="password" class="form-control" placeholder="Password" required>
                    <input name="DoorKeeper[password_repeat]" type="password" class="form-control" placeholder="Confirm Password" required>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Create Account</button>
            </form> 
        </div> <!-- /signup -->

    </div> <!-- /tab-content -->
</div>

<div class="alert-error <?php if ($errorMessage == '') echo 'hide'; ?>">
    <?php echo $errorMessage; ?>
</div>
