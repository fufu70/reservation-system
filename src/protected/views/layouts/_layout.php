<?php
	/**
	 * The layout represents the basics of a layout, meaning that it has a header and a body.
	 *
	 * @author Christian Micklisch <cmicklis@stetson.edu>
	 */
	
	if (Yii::app()->user->isBranchSelected())
	{
		$timeZone = Setting::model()->currentBranch()->type(SettingType::TIME_ZONE)->find()->setting_value;
		date_default_timezone_set($timeZone);
	}

	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->clientScript;

	// CSS Files
	$cs->registerCssFile($baseUrl . '/css/footable.core.css');
	$cs->registerCssFile($baseUrl . '/css/searchbar.css');

	// Javascript Files
	$cs->registerCoreScript('jquery');
	$cs->registerScriptFile($baseUrl . '/js/bootstrap.min.js');
	$cs->registerScriptFile($baseUrl . '/js/bootstrap-wizard.js');
	$cs->registerScriptFile($baseUrl . '/js/chosen.min.js');
	$cs->registerScriptFile($baseUrl . '/js/footable.js');
	$cs->registerScriptFile($baseUrl . '/js/footable.paginate.js');
	$cs->registerScriptFile($baseUrl . '/js/footable.sort.js');
	$cs->registerScriptFile($baseUrl . '/js/site/ajaxFilter.js');
	$cs->registerScriptFile($baseUrl . '/js/site/global.js');
?>

<!DOCTYPE>
<html lang="en">
	<?php
		$this->renderPartial('/layouts/_header');

		echo '<body>';

		if (!empty($displaySidebar))
		{
			echo '<div id="content-container" class="content-container">';

			$this->renderPartial('/layouts/_sidebar');

			echo '<div class="pusher">';
			echo '<div class="content">';
			if (!empty($displayNavbar))
			{
				$this->renderPartial('/layouts/_navbar');
			}
			echo '<div class="main clearfix">';
		}

		echo $content;

		// closing tags for the sidebar
		if (!empty($displaySidebar))
		{
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '<div class="pusher-overlay"></div>';
			echo '</div>';
		}

		$this->renderPartial('/layouts/_dynamic-labels');

		echo '</body>';

		if (!empty($displayFooter))
		{
			$this->renderPartial('/layouts/_footer');
		}
	?>
</html>
