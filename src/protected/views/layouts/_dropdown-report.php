<?php
	$items = array();

	if (Yii::app()->user->hasAccess('report/reservation'))
	{
		$items[] = array(
			'label' => 'Reservation',
			'url' => array('report/reservation'),
		);
	}

	if (Yii::app()->user->hasAccess('report/item'))
	{
		$items[] = array(
			'label' => 'Item',
			'url' => array('report/item'),
			'itemOptions' => array('class' => 'dynamic-label')
		);
	}

	if (empty($items))
		return;

	$this->widget('DropdownMenu', array(
		'iconClass' => 'fa fa-file-text',
		'label' => 'Reports',
		'items' => $items
	));
?>
