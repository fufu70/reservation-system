<?php 
	if (!Yii::app()->user->isBranchSelected())
		return;

	$itemName = Setting::model()->type(SettingType::ITEM_NAME)->currentBranch()->find()->setting_value;
?>

<script type="text/javascript">
	
	var itemName = '<?php echo $itemName ?>';

	function processDynamicLabels()
	{
		$('.dynamic-label').each(function() {
			$(this).html($(this).html().replace(/Item/g, itemName));
		});
	}

	processDynamicLabels();

</script>