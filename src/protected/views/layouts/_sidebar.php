<nav class="sidebar sidebar-transition">
	<ul class="sidebar-nav" id="accordion_menu">
		<li class="sidebar-header" id="close-menu">
			<?php
				$text = '<i class="fa fa-times"></i>Close';
				echo CHtml::link($text);
			?>
		</li>
		<?php
			$this->renderPartial('/layouts/_dropdown-reservation');
			$this->renderPartial('/layouts/_dropdown-report');
			$this->renderPartial('/layouts/_dropdown-manage');
			$this->renderPartial('/layouts/_dropdown-account');
		?>
		<li>
			<?php
				$text = '<i class="fa fa-exchange"></i>Select Branch';
				echo CHtml::link($text, array('branch/select'));
			?>
		</li>
		<li>
			<?php
				$text = '<i class="fa fa-sign-out"></i>Log Out';
				echo CHtml::link($text, array('doorkeeper/logout'));
			?>
		</li>
	</ul>
</nav>
