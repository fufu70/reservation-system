<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/site/searchbar.js');
?>

<li class="navbar-form">
	<div class="form-group">
		<a href="#" id="search-button">
			<span class="fava glyphicon glyphicon-search" aria-hidden="true"></span>
		</a>
		<input type="text" id="search-bar" style="display: none;" class="form-control" placeholder="Search">
	</div>
</li>
