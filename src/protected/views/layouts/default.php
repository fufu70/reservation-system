<?php
	/**
	 * The navbar is automatically generated from the access list of only displayable access points 
	 * and the container for the content has top and bottom margins so that the navbar does not cover 
	 * the content.
	 */
	
	// CSS Files
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/less/global.min.css');

	$content = '<div class="container-fluid main-container">' . $content . '</div>';
	
	$this->renderPartial('/layouts/_layout', array(
		'content' => $content,
		'displayNavbar' => true,
		'displaySidebar' => true,
		'displayFooter' => false
	));
?>