<?php
	/**
	 * The layout for when the user is loggin in, the container will be animated and the css 
	 * is specified in the less files. No navbar is displayed.
	 */

	// CSS Files
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/less/doorkeeper.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/animate.min.css');

	$content = '<div class="doorkeeper animated slideInDown">' . $content . '</div>';
	
	$this->renderPartial('/layouts/_layout', array(
		'content' => $content,
		'displayNavbar' => false,
	));
?>
