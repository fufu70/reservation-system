<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<ul class="nav navbar-nav">
			<li>
				<a href="#" id="menu-toggle"><i class="fa fa-bars"></i></a>
			</li>
			<li>
				<?php echo CHtml::link(Yii::app()->name, array('branch/'), array('class' => 'navbar-brand')); ?>
			</li>
		</ul>
		<ul class="nav navbar-nav pull-right">
			<?php
				// if (Yii::app()->user->hasAccess('search/search'))
				// {
				// 	$this->renderPartial('/layouts/_searchbar');
				// }
			?>
			<li>
				<?php echo CHtml::link('<strong>' . Yii::app()->user->name . '</strong>', array('account/')); ?>
			</li>
			<li class="hidden-xs">
				<?php echo CHtml::link('<i class="fa fa-sign-out"></i>Log Out', array('doorkeeper/logout')); ?>
			</li>
		</ul>
	</div>
</nav>
