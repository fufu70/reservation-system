<?php
	if (Yii::app()->user->hasAccess('reservation/display'))
	{
		echo '<li>';
		$text = '<i class="fa fa-clock-o"></i>Reservations';
		echo CHtml::link($text, array('reservation/display'));
		echo '</li>';
	}
?>
