<?php
	$items = array();

	if (Yii::app()->user->hasAccess('item/display'))
	{
		$items[] = array(
			'label' => 'Item',
			'url' => array('item/display'),
			'itemOptions' => array('class' => 'dynamic-label')
		);
	}

	if (Yii::app()->user->hasAccess('itemStatus/display'))
	{
		$items[] = array(
			'label' => 'Item Status',
			'url' => array('itemStatus/display'),
			'itemOptions' => array('class' => 'dynamic-label')
		);
	}

	if (Yii::app()->user->hasAccess('itemType/display'))
	{
		$items[] = array(
			'label' => 'Item Type',
			'url' => array('itemType/display'),
			'itemOptions' => array('class' => 'dynamic-label')
		);
	}

	if (Yii::app()->user->hasAccess('location/display'))
	{
		$items[] = array(
			'label' => 'Location',
			'url' => array('location/display'),
		);
	}

	if (Yii::app()->user->hasAccess('user/display'))
	{
		$items[] = array(
			'label' => 'User',
			'url' => array('user/display'),
		);
	}

	if (empty($items))
		return;

	$this->widget('DropdownMenu', array(
		'iconClass' => 'fa fa-wrench',
		'label' => 'Manage',
		'items' => $items,
	));
?>
