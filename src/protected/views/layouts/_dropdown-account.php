<?php
	$items = array();

	$items[] = array(
		'label' => 'Account',
		'url' => array('account/')
	);

	foreach (Yii::app()->user->access_list as $access)
	{
		if ($access->controller_name == 'branch' && $access->visible)
		{
			$items[] = array(
				'label' => $access->display_name,
				'url' => array($access->route),
			);
		}
	}

	asort($items);

	$this->widget('DropdownMenu', array(
		'iconClass' => 'fa fa-cogs',
		'label' => 'Settings',
		'items' => $items,
	));
?>
