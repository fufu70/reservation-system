<footer class="footer">
	<div class="fluid-container">
		<div class="pull-left text-muted small hidden-xs">
			<?php
				echo Yii::app()->params->copyrightInfo;
			?>
		</div>
		<div class="pull-right text-muted">
			<?php
				$branch = Yii::app()->user->branch;
				echo $branch->system->system_name . ' - ' . $branch->branch_name;
			?>
		</div>
	</div>
</footer>
