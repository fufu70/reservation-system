
<div class="col-xs-12 col-md-8 col-lg-6">
	<div class="form-group">
		<span class="h3">Branch Settings</span>
	</div>
	<div class="panel panel-default">
		<div class="panel-body">
			<form action="/branch/settings" method="post">
				<table class="table" data-filter="#filter" data-filter-text-only="true" data-sort="false">
					<thead>
						<tr>
							<th class="fit">Setting Name</th>
							<th>Value</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($settingList as $setting)
							{
								echo '<tr>';
								echo '<td class="fit">' . $setting->setting_name . '</td>';
								echo '<td>';

								$id = $setting->setting_id;
								$value = $setting->setting_value;
								$path = $setting->settingType->inputType->getPathToWidget();

								$name = 'Setting[' . $id . ']';

								$this->widget($path, array(
									'name' => $name,
									'value' => $value,
								));

								echo '</td>';
								echo '</tr>';
							}
						?>
					</tbody>
				</table>
				<div class="row">
					<div class="col-xs-4 col-xs-offset-8">
						<div class="btn-group btn-group-justified">
							<div class="btn-group">
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
