<?php
	/**
	 * This view shows all access points for the current user and which user roles 
	 * have access. The user can change which user roles have access to each action.
	 * 
	 * Access points are sorted into panels by their controller name.
	 */
	
	$accessList = Access::model()
		->currentBranch()
		->userRole(Yii::app()->user->role)
		->visible()
		->findAll();

	$userRoleList = UserRole::model()
		->maxRank(Yii::app()->user->role)
		->currentBranch()
		->findAll();

	$controllerList = array();

	// Find all controller names
	foreach ($accessList as $access)
	{
		if (!in_array($access->controller_name, $controllerList))
		{
			$controllerList[ $access->controller_name ] = $access->controllerLabel;
		}
	}

	asort($controllerList);
?>

<div class="col-md-6">
	<div class="form-group">
		<span class="h3">Role Management</span>
	</div>
	<form action="/branch/access" method="post">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<?php

			// create a panel for each controller
			foreach ($controllerList as $controllerName => $controllerLabel)
			{
				echo '<div class="panel panel-default">';

				// start panel header
				echo '<div class="panel-heading pointer" role="tab" data-toggle="collapse" data-parent="#accordion" href="#collapse' . $controllerName . '">';
				echo '<h4 class="panel-title dynamic-label">' . $controllerLabel . '</h4>';		        		
				echo '</div>';
				// end panel header

				// start panel content
				echo '<div id="collapse' . $controllerName . '" class="panel-collapse collapse" role="tabpanel">';
				echo '<div class="panel-body">';

			    // start table
		        echo '<table class="table table-hover">';
		        echo '<thead>
		        		<tr>
		        			<th class="fit">Access Point</th>
		        			<th class="fit text-right">User Roles</th>
		        		</tr>
		        	</thead>';
		        echo '<tbody>';

				foreach ($accessList as $access)
				{
		        	if ($access->controller_name == $controllerName)
		        	{
		        		echo '<tr>';
			        	echo '<td class="dynamic-label" style="vertical-align: middle">' . $access->display_name . '</td>';
			        	echo '<td><div class="btn-group pull-right" data-toggle="buttons">';

			        	foreach ($userRoleList as $userRole)
			        	{
			        		$enabled = Access::model()
			        			->currentBranch()
			        			->userRole($userRole->user_role_name)
			        			->controller($access->controller_name)
			        			->action($access->action_name)
			        			->exists();

			        		$active = ($enabled) ? 'active' : '';
			        		$name = 'Access[' . $access->controller_name . '][' . $access->action_name . '][' . $userRole->user_role_name . ']';

			        		echo CHtml::openTag('label', array('class' => 'btn btn-default btn-sm ' . $active));

			        		echo $userRole->user_role_name;

			        		echo CHtml::hiddenField($name, '0');
			        		echo CHtml::checkBox($name, $enabled, array('autocomplete' => 'off'));
			        		echo CHtml::closeTag('label');
			        	}

			        	echo '</div></td>';
			            echo '</tr>';
		        	}
				}
				echo '</tbody></table>';
				// end table
				
				echo '<div class="text-right"><button class="btn btn-primary" type="submit">Submit</button></div>';
				echo '</div></div>';
				// end panel content

				echo '</div>';
			}

			?>
		</div>
	</form>
</div>
