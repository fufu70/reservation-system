<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/moment.min.js');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/fullcalendar.min.js');

	$colorSettings = Setting::model()
		->currentBranch()
		->type(SettingType::RESERVATION_STATUS_COLOR)
		->findAll();

	$resStatusColors = CHtml::listData($colorSettings, 'setting_attribute', 'setting_value');
?>

<div class="col-xs-12">
	<div id="status_buttons" class="btn-group btn-group-justified" data-toggle="buttons">
		<label class="btn btn-default">
			<input type="checkbox"><span style="color: <?php echo $resStatusColors['Pending']; ?>">Pending</span></input>
		</label>
		<label class="btn btn-default">
			<input type="checkbox"><span style="color: <?php echo $resStatusColors['Ready']; ?>">Ready</span></input>
		</label>
		<label class="btn btn-default">
			<input type="checkbox"><span style="color: <?php echo $resStatusColors['Active']; ?>">Active</span></input>
		</label>
		<label class="btn btn-default">
			<input type="checkbox"><span style="color: <?php echo $resStatusColors['Overdue']; ?>">Overdue</span></input>
		</label>
		<label class="btn btn-default">
			<input type="checkbox"><span style="color: <?php echo $resStatusColors['Closed']; ?>">Closed</span></input>
		</label>
		<label class="btn btn-default">
			<input type="checkbox"><span style="color: <?php echo $resStatusColors['Canceled']; ?>">Canceled</span></input>
		</label>
	</div>
	<div class="margin-small">
		<div id='calendar' style='font-size: 13px' class="fc fc-ltr"></div>
	</div>
</div>

<script type="text/javascript">
	jQuery(function ($) {

		$('#status_buttons').find('label').tooltip({
			animation: false,
			placement: 'bottom',
			delay: { show: 0, hide: 0 },
			trigger: 'hover',
			container: 'body',
		});

		$.each($('#status_buttons').find('label'), function (i, label) {
			$(label).data('bs.tooltip').options.title = function () {
				var input = $(label).find('input');
				var status = input.siblings('span').html();
				if (input.is(':checked')) {
					return 'Show ' + status + ' Reservations';
				} else {
					return 'Hide ' + status + ' Reservations';
				}
			};
		});

		$('#status_buttons').find('input').change(function () {
			var status = $(this).siblings('span').html();
			toggleReservation(!this.checked, status);
			$(this).closest('label').tooltip('hide');
			$(this).closest('label').tooltip('show');
		});

		// Sets the visibility of all reservations of a given status
		function toggleReservation(visible, status) {
			var eventList = $('#calendar').fullCalendar('clientEvents', function (calEvent) {
				return calEvent.status == status;
			});
			for (var i = 0; i < eventList.length; i++) {
				eventList[i].visible = visible;
			}
			$('#calendar').fullCalendar('rerenderEvents');
		}
	});

	$('#calendar').fullCalendar({
		
		// Fetches and shows reservation details when an event is clicked
		eventClick: function (calEvent, jsEvent, view) {

			$.ajax({
				type: 'GET',
				url: '/reservation/fetchDetails',
				data: { 'id': calEvent.id },
				dataType: 'html',
				success: function (html) {
					$('#reservation_details').remove();
					$('body').children('.modal-backdrop').remove();
					$('body').append(html);
					processDynamicLabels();
					$('#reservation_details').modal('show');
				}
			});
		},
	  
		// Only renders events that are visible
		eventRender: function (event, element) {
			return event.visible;
		},

		viewRender: function (view, element) {
			$('#calendar').fullCalendar('getView').calendar.options.eventLimit = (view.type == 'month') ? 6 : false;
		},
			
		// Reduces the font-size of the title when the window is too small
		windowResize: function (view) {
			if ($('#calendar').width() < 600) {
				$('.fc-center h2').css({ 'font-size': '1.5em' });
			} else {
				$('.fc-center h2').css({ 'font-size': '2.4em' });
			}
		},

		header: {
			left: 'prev,next,today',
			center: 'title',
			right: 'month,basicWeek,agendaDay'
		},

		buttonText: {
			today: 'Today',
			month: 'Month',
			week: 'Week',
			day: 'Day',
		},

		// Disable drag and drop
		editable: false,

		events: '/reservation/fetchReservations',
	});
</script> 
