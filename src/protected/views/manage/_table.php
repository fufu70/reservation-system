<?php
	/**
	 * @param CModel 			$model 	The model used to build the columns of the table.
	 * @param array<CModel> 	$rows 	The list of models used to build the rows of the table.
	 */

	$columns = ($model instanceof CustomEntity) ? $model->allAttributes : $model->attributes;
	$inputTypes = $model->inputTypes();
?>

<div id="entity_modal"></div>

<table class="table table-hover footable">
	<thead>
		<tr>
			<th data-sort-ignore="true"></th>

			<?php
				foreach ($columns as $name => $val)
				{
					if (isset($inputTypes[$name]) && $inputTypes[$name] != 'hidden')
					{
						echo '<th class="fit">' . CHtml::encode($model->getAttributeLabel($name)) . '</th>';
					}
					else
					{
						unset($columns[$name]);
					}
				}
			?>

			<th class="expand" data-sort-ignore="true"></th>
		</tr>
	</thead>
	<tbody>

		<?php
			foreach ($rows as $row)
			{
				echo '<tr>';

				// Start button group column
				echo '<td class="fit">';
				echo '<div class="btn-group" role="group">';

				$success = 'js:function(html) {
					$("#entity_modal").html(html);
					initBehavior($("#entity_modal"));
					processDynamicLabels();
					$("#entity_modal").find(".modal").modal("show");
				}';

				// Configure edit button
				$icon = '&nbsp;<span class="glyphicon glyphicon-pencil"></span>&nbsp;';
				$htmlOptions = array(
					'title' => 'Edit',
					'class' => 'btn btn-xs btn-default',
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
				);

				echo CHtml::ajaxLink($icon,
					array(get_class($row) . '/update', 'id' => $row->primaryKey),
					array('success' => $success),
					$htmlOptions
				);

				// Configure delete button
				$icon = '&nbsp;<span class="glyphicon glyphicon-remove"></span>&nbsp;';
				$htmlOptions = array(
					'title' => 'Delete',
					'class' => 'btn btn-xs btn-danger',
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
				);

				echo CHtml::ajaxLink($icon,
					array(get_class($row) . '/remove', 'id' => $row->primaryKey),
					array('success' => $success),
					$htmlOptions
				);

				echo '</div>';
				echo '</td>';
				// End button group column
				
				$displays = $row->attributeDisplays();

				foreach ($columns as $name => $val)
				{
					echo '<td class="fit">';
					echo (isset($displays[$name])) ? CHtml::encode($displays[$name]) : CHtml::encode($row->$name);
					echo '</td>';
				}

				echo '<td></td>';
				echo '</tr>';
			}
		?>

	</tbody>
</table>
