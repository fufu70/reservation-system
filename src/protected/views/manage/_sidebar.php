<?php
	$items = array();

	if (Yii::app()->user->hasAccess('item/display'))
	{
		$items[] = array(
			'label' => 'Item',
			'url' => array('item/display'),
			'active' => $this->id == 'item',
			'itemOptions' => array('class' => 'dynamic-label')
		);
	}

	if (Yii::app()->user->hasAccess('itemStatus/display'))
	{
		$items[] = array(
			'label' => 'Item Status',
			'url' => array('itemStatus/display'),
			'active' => $this->id == 'itemStatus',
			'itemOptions' => array('class' => 'dynamic-label')
		);
	}

	if (Yii::app()->user->hasAccess('itemType/display'))
	{
		$items[] = array(
			'label' => 'Item Type',
			'url' => array('itemType/display'),
			'active' => $this->id == 'itemType',
			'itemOptions' => array('class' => 'dynamic-label')
		);
	}

	if (Yii::app()->user->hasAccess('location/display'))
	{
		$items[] = array(
			'label' => 'Location',
			'url' => array('location/display'),
			'active' => $this->id == 'location',
		);
	}

	if (Yii::app()->user->hasAccess('user/display'))
	{
		$items[] = array(
			'label' => 'User',
			'url' => array('user/display'),
			'active' => $this->id == 'user',
		);
	}

	$this->widget('SidebarMenu', array('items' => $items));
?>
