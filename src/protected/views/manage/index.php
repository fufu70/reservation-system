<?php
	/**
	 * The manager view displays a table of all entities of a given type, and displays forms for creating
	 * and updating entities.
	 *
	 * @param CModel 			$model 	    The entity to create the forms for.
	 * @param array<CModel> 	$rows 	    The entity rows to display.
	 * @param boolean 			$success 	Whether to show an alert for successful form submission.
	 */
	
	// Create a form for the entity.
	$config = array(
		'title' => "Create {$model->entityType}",
		'action' => $this->createUrl('create'),
	);

	$entityform = new AutoForm($config, $model);

	$this->renderPartial('/global/_form-modal', array(
		'form' => $entityform,
		'id' => 'entity_form',
	));

	// Render form for creating field types.
	$this->renderPartial('/manage/_create-field-type', array(
		'id' => 'create_field_type',
		'entityType' => $model->entityType,
	));

	// Render form for updating field types.
	$this->renderPartial('/manage/_update-field-type', array(
		'id' => 'update_field_type',
		'entityType' => $model->entityType,
	));
?>

<div class="col-xs-12 form-group">
	<span class="h3">Manage</span>
</div>
<div class="col-xs-12 col-sm-3 dynamic-label">
	<?php
		$this->renderPartial('/manage/_sidebar');
	?>
</div>
<div class="col-xs-12 col-sm-9">
	<div class="row">
		<div class="col-xs-12">
			<div class="alert alert-success <?php if (empty($success)) echo 'hide'; ?>" role="alert">
				<strong>Success!</strong> The form was submitted.
			</div>
			<div class="form-group btn-group dynamic-label" role="group">
				<?php
					if (Yii::app()->user->hasAccess($entityform->action))
					{
						echo CHtml::button($entityform->title, array(
							'class' => 'btn btn-primary btn-sm',
							'data-target' => '#entity_form',
							'data-toggle' => 'modal',
						));
					}

					if (Yii::app()->user->hasAccess('fieldType/create'))
					{
						echo CHtml::button('Create Field', array(
							'class' => 'btn btn-default btn-sm',
							'data-target' => '#create_field_type',
							'data-toggle' => 'modal',
						));
					}

					if (Yii::app()->user->hasAccess('fieldType/update'))
					{
						echo CHtml::button('Edit Field', array(
							'class' => 'btn btn-default btn-sm',
							'data-target' => '#update_field_type',
							'data-toggle' => 'modal',
						));
					}
				?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default scroll-x">
				<?php				
					$this->renderPartial('/manage/_table', array(
						'model' => $model,
						'rows' => $rows,
					));
				?>
			</div>
		</div>
	</div>
</div>
