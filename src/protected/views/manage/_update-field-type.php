<?php
	/**
	 * This view renders a form for updating entity fields.
	 *
	 * @param string 	$id 			The ID of the form container. Optional.
	 * @param string    $entityType     The entity type to be associated with the form.
	 */
	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/site/updateFieldType.js', CClientScript::POS_END);

	$config = array(
		'activeForm' => array(
			'class' => 'CActiveForm',
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => true,
				'validateOnType' => false,
			),
		),
		'title' => 'Edit Field',
		'action' => $this->createUrl('fieldType/update'),
		'elements' => array(
			'field_type_id' => array(
				'type' => InputType::model()->getPathToWidget(InputType::FIELD_TYPE),
				'entityType' => $entityType,
				'label' => 'Select A Field',
			),
			'field_type_name' => array(
				'type' => InputType::model()->getPathToWidget(InputType::TEXT),
			),
			'description' => array(
				'type' => InputType::model()->getPathToWidget(InputType::TEXT_AREA),
			),
		),
	);

	$model = new FieldType('update');
	$form = new CForm($config, $model);

	$this->renderPartial('/global/_form-modal', array(
		'form' => $form,
		'id' => $id,
	));
?>
