<?php
	/**
	 * This view renders a form for creating new entity fields.
	 *
	 * @param string 	$id 			The ID of the form container. Optional.
	 * @param string    $entityType     The entity type to be associated with the form.
	 */
	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/site/createFieldType.js', CClientScript::POS_END);

	$config = array(
		'title' => 'Create Field',
		'description' => "Complete the form to create a new {$entityType} field.",
		'action' => $this->createUrl('fieldType/create'),
	);

	$model = new FieldType;
	$model->entity_type_name = $entityType;

	$form = new AutoForm($config, $model);
?>

<div class="modal fade" id="<?php if (isset($id)) echo $id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<?php echo $form->renderBegin(); ?>

			<div class="modal-header dynamic-label">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?php echo $form->title; ?></h4>
				<p class="text-muted"><?php echo $form->description; ?></p>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-6 form-group">
						<?php echo $form->elements['entity_type_name']->render(); ?>
						<?php echo $form->elements['field_type_name']->render(); ?>
					</div>
					<div class="col-xs-6 form-group">
						<?php echo $form->elements['input_type_id']->render(); ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo $form->elements['description']->render(); ?>
				</div>
			</div>
			<div class="modal-footer-extend">
				<h4>Preview</h4>
				<div class="row">
					<div class="col-xs-6 col-xs-offset-3 form-group">
						<label id="field_name_preview">Field Name</label>
						<div id="field_input_preview">
							<input type="text" class="form-control">
						</div>
						<div class="hint" id="field_description_preview" style="display:none"></div>					
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="btn-group btn-group-justified">
					<div class="btn-group">
						<?php echo CHtml::button('Cancel', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')); ?>
					</div>
					<div class="btn-group">
						<?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
					</div>
				</div>
			</div>

			<?php echo $form->renderEnd(); ?>
			
		</div>
	</div>
</div>
