
<div class="col-xs-12">
    
    <!-- No Results Alert -->
    <div class="alert alert-warning <?php if (count($rows) > 0) { echo 'hide'; } ?>">
        No results found.
    </div>

    <!-- Results Table -->
    <div class="<?php if (count($rows) == 0) echo 'hide'; ?>">
        <div class="panel panel-default scroll-hor">
            <?php
                if (count($rows) > 0)
                {
                    $this->renderPartial('/manage/_table', array(
                        'model' => $rows[0],
                        'rows' => $rows,
                    ));
                }
            ?>
        </div>
        <form class="report-form" action="/" method="post">
            <input name="Report[download]" type="text" class="hide" value="1">
            <button class="btn btn-primary" type="submit">Download CSV</button>
        </form>
    </div>

</div>
