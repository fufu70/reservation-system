<?php
	$model = new Reservation;
	$inputTypes = $model->inputTypes();
	$comparableList = CHtml::listData(InputType::model()->findAll(), 'pathToWidget', 'comparable');

	$criteriaList = array(
		array(
			'label' => 'User',
			'attribute' => 'user_id',
			'inputType' => InputType::model()->getPathToWidget(InputType::USER),
			'comparable' => false,
			'limit' => 3,
		),
		array(
			'label' => 'Location',
			'attribute' => 'location_id',
			'inputType' => InputType::model()->getPathToWidget(InputType::LOCATION),
			'comparable' => false,
			'limit' => 3,
		),
		array(
			'label' => 'Reservation_Status',
			'attribute' => Report::SCOPE_IDENTIFIER . 'status',
			'inputType' => InputType::model()->getPathToWidget(InputType::RESERVATION_STATUS),
			'comparable' => false,
			'limit' => 3,
		),
		array(
			'label' => 'Date_Range',
			'attribute' => Report::SCOPE_IDENTIFIER . 'dateRange',
			'inputType' => InputType::model()->getPathToWidget(InputType::DATE_TIME),
			'comparable' => true,
			'limit' => 3,
		),
		array(
			'label' => 'Item_Type',
			'attribute' => Report::SCOPE_IDENTIFIER . 'itemType',
			'inputType' => InputType::model()->getPathToWidget(InputType::ITEM_TYPE),
			'comparable' => false,
			'limit' => 3,
		),
		array(
			'label' => 'Item_Status',
			'attribute' => Report::SCOPE_IDENTIFIER . 'itemStatus',
			'inputType' => InputType::model()->getPathToWidget(InputType::ITEM_STATUS),
			'comparable' => false,
			'limit' => 3,
		),
	);

	foreach (array_keys($model->getFields()) as $name)
	{
		$criteriaList[] = array(
			'label' => $name,
			'attribute' => $name,
			'inputType' => $inputTypes[ $name ],
			'comparable' => $comparableList[ $inputTypes[ $name ] ],
			'limit' => 1,
		);
	}

	$this->renderPartial('_search', array(
		'title' => 'Search Reservations',
		'action' => $this->createUrl('report/reservation'),
		'criteriaList' => $criteriaList,
	));
?>
