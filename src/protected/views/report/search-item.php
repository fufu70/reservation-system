<?php
	$model = new Item;
	$inputTypes = $model->inputTypes();
	$comparableList = CHtml::listData(InputType::model()->findAll(), 'pathToWidget', 'comparable');

	$criteriaList = array(
		array(
			'label' => 'Item_Status',
			'attribute' => 'item_status_id',
			'inputType' => InputType::model()->getPathToWidget(InputType::ITEM_STATUS),
			'comparable' => false,
			'limit' => 3,
		),
		array(
			'label' => 'Item_Type',
			'attribute' => 'item_type_id',
			'inputType' => InputType::model()->getPathToWidget(InputType::ITEM_TYPE),
			'comparable' => false,
			'limit' => 3,
		),
		array(
			'label' => 'Reserved',
			'attribute' => Report::SCOPE_IDENTIFIER . 'reserved',
			'inputType' => InputType::model()->getPathToWidget(InputType::DATE_TIME),
			'comparable' => true,
			'limit' => 3,
		),
	);

	foreach (array_keys($model->getFields()) as $name)
	{
		$criteriaList[] = array(
			'label' => $name,
			'attribute' => $name,
			'inputType' => $inputTypes[ $name ],
			'comparable' => $comparableList[ $inputTypes[ $name ] ],
			'limit' => 1,
		);
	}

	$this->renderPartial('_search', array(
		'title' => 'Search Items',
		'action' => $this->createUrl('report/item'),
		'criteriaList' => $criteriaList,
	));
?>
