<?php
	/**
	 * Renders a search criteria input for a single attribute.
	 * 
	 * @param string  $attribute    The name of the attribute.
	 * @param string  $inputType    The path to the input widget.
	 * @param boolean $comparable   Whether the input is comparable.
	 */
	
	$label = (isset($label)) ? $label : ucwords(str_replace(array('-', '_', '.'), ' ', $attribute));

	$inputName = 'Criteria[' . $attribute . ']';
?>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-4">
				<div class="input-group">
					<span class="input-group-btn">
						<button type="button" class="close close-panel" style="margin-right: 15px">&times;</button>
					</span>
					<p class="criteria-label dynamic-label form-control-static"><?php echo $label; ?></p>
				</div>
			</div>
			<div class="col-xs-8">
				<div class="row">
					<?php
						if ($comparable)
						{
							echo '<div class="col-xs-5">';
							$this->widget($inputType, array('name' => $inputName . '[from][]'));
							echo '</div>';

							echo '<div class="col-xs-2">';
							echo '<p class="form-control-static text-center">to</p>';
							echo '</div>';

							echo '<div class="col-xs-5">';
							$this->widget($inputType, array('name' => $inputName . '[to][]'));
							echo '</div>';
						}
						else
						{
							echo '<div class="col-xs-6">';
							echo '<p class="form-control-static">equals</p>';
							echo '</div>';

							echo '<div class="col-xs-6">';
							$this->widget($inputType, array('name' => $inputName . '[value][]'));
							echo '</div>';
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
