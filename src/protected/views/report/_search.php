<?php
	/**
	 * Renders a customizable search form using a set of criteria.
	 * 
	 * @param string $title          The title of the form.
	 * @param string $action         The action of the form.
	 * @param array  $criteriaList   The list of available search criteria.
	 */
	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/site/report.js', CClientScript::POS_END);

	$ajaxOptions = array('success' => 'js:addCriteria');
	$htmlOptions = array('class' => 'list-group-item dynamic-label');

	asort($criteriaList);

	$limits = array();

	foreach ($criteriaList as $criteria)
	{
		$label = ucwords(str_replace('_', ' ', $criteria['label']));
		$limits[ $label ] = $criteria['limit'];
		unset($criteria['limit']);
	}
?>

<script>
    var limits = <?php echo json_encode($limits); ?>;
</script>

<div class="col-xs-12 form-group">
	<span class="h3 dynamic-label"><?php echo $title; ?></span>
</div>
<div class="col-xs-12 col-sm-3">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Add Criteria</h3>
		</div>
		<div id="search_criteria" class="panel-body list-group">
			<?php
				foreach ($criteriaList as $criteria)
				{
					$url = array_merge(array('report/fetchCriteria'), $criteria);
					$label = ucwords(str_replace('_', ' ', $criteria['label']));

					echo CHtml::ajaxLink($label, $url, $ajaxOptions, $htmlOptions);
				}
			?>
		</div>
	</div>
</div>
<div class="col-xs-12 col-sm-9">
	<form id="search_form" action="<?php echo $action; ?>" method="post" class="form-horizontal">
		<?php
			echo CHtml::submitButton('Search', array('class' => 'btn btn-primary pull-right'));
		?>
	</form>
</div>
