
<div class="col-xs-12 form-group">
	<span class="h3">Account</span>
</div>
<div class="col-xs-12 col-sm-3">
	<?php
		$this->renderPartial('/account/_sidebar');
	?>
</div>
<div class="col-xs-12 col-sm-5">

	<div class="alert alert-success <?php if (empty($success)) echo 'hide'; ?>" role="alert">
		<strong>Success!</strong> Your account has been updated.
	</div>

	<div class="panel panel-default">
		<div class="panel-body">
			<?php
				$this->renderPartial($formView);
			?>
		</div>
	</div>	
</div>
