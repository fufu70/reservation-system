<?php
	$model = new ChangePasswordForm;

	$config = array(
		'title' => 'Change Password',
		'action' => $this->createUrl('account/changePassword'),
		'activeForm' => array(
			'class' => 'CActiveForm',
			'enableAjaxValidation' => true,
			'enableClientValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => false,
				'validateOnType' => false,
			),
		),
		'elements' => array(
			'current_password' => array('type' => 'password'),
			'new_password' => array('type' => 'password'),
			'confirm_password' => array('type' => 'password'),
		),
	);

	$form = new CForm($config, $model);

	echo $form->renderBegin();

	foreach ($form->getElements() as $element)
	{
		echo '<div class="form-group">' . $element->render() . '</div>';
	}

	echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary'));

	echo $form->renderEnd();
?>

