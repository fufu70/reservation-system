<?php
	$items = array(
		array(
			'label' => 'Personal Settings',
			'url' => array('account/personalSettings'),
		),
		array(
			'label' => 'Change Password',
			'url' => array('account/changePassword'),
		),
	);

	$this->widget('SidebarMenu', array('items' => $items));
?>
