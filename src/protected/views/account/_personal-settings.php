<?php
	$user = User::model()->findByPK(Yii::app()->user->id);

	$config = array(
		'title' => 'Personal Settings',
		'action' => $this->createUrl('account/personalSettings'),
	);

	$form = new AutoForm($config, $user);

	echo $form->renderBegin();

	foreach ($form->getElements() as $element)
	{
		if ($element->type == 'hidden')
		{
			echo $element->render();
		}
		else
		{
			echo '<div class="form-group">' . $element->render() . '</div>';
		}
	}

	echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary'));

	echo $form->renderEnd();
?>
