<?php
	/**
	 * This view renders a single column form embedded in a panel.
	 *
	 * @param CForm 	$form 			The form object to construct the view for.
	 * @param string 	$id 			The ID of the form container. Optional.
	 */
?>

<div class="panel panel-default" id="<?php if (isset($id)) echo $id; ?>">
	<div class="panel-heading dynamic-label">
		<h3 class="panel-title"><?php echo $form->title; ?></h3>
	</div>
	<div class="panel-body">
  		<?php
			echo $form->renderBegin();

			// Render each form element.
			foreach ($form->getElements() as $element)
			{
	  			if ($element->type == 'hidden')
				{
					echo $element->render();
				}
				else
				{
					echo '<div class="form-group">' . $element->render() . '</div>';
	  			}
			}

			echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary'));

			echo $form->renderEnd(); 
  		?>
	</div>
</div>
