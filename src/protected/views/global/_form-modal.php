<?php
	/**
	 * This view renders a two column form embedded in a modal.
	 *
	 * @param CForm 	$form 			The form object to construct the view for.
	 * @param string 	$id 			The ID of the form container. Optional.
	 */
?>

<div class="modal fade" id="<?php if (isset($id)) echo $id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<?php echo $form->renderBegin(); ?>

			<div class="modal-header dynamic-label">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?php echo $form->title; ?></h4>
				<p class="text-muted"><?php echo $form->description; ?></p>
			</div>
			<div class="modal-body">
				<?php
					$this->renderPartial('/global/_two-column-elements', array(
						'elements' => $form->getElements(),
					));
				?>
			</div>
			<div class="modal-footer">
				<div class="btn-group btn-group-justified">
					<div class="btn-group">
						<?php echo CHtml::button('Cancel', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')); ?>
					</div>
					<div class="btn-group">
						<?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
					</div>
				</div>
			</div>

			<?php echo $form->renderEnd(); ?>
			
		</div>
	</div>
</div>
