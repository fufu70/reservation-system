<?php
	/**
	 * This view renders a two column layout of form input elements.
	 *
	 * @param CFormElementCollection 	$elements
	 */
	
	$textArea = InputType::model()->getPathToWidget(InputType::TEXT_AREA);
	$item = InputType::model()->getPathToWidget(InputType::ITEM);
	
	$keys = $elements->keys;
	$i = 0;

	// Render each form element.
	while ($i < count($keys))
	{
		$element = $elements[$keys[$i]];

		if ($element instanceof CFormStringElement || $element->type == $textArea || $element->type == $item)
		{
			echo '<div class="form-group">' . $element->render() . '</div>';
			$i ++;
		}
		else if ($element->type == 'hidden')
		{
			echo $element->render();
			$i ++;
		}
		else
		{
			// Render two form elements on the same row.
			echo '<div class="row">';
			echo '<div class="col-sm-6 form-group">' . $element->render() . '</div>';

			if ($i + 1 < count($keys))
			{
				$element = $elements[$keys[$i + 1]];
				if ($element->type != $textArea && $element->type != 'hidden' && $element->type != $item)
				{
					echo '<div class="col-sm-6 form-group">' . $element->render() . '</div>';
					$i ++;
				}
			}

			echo '</div>';
			$i ++;
		}
	}
?>
