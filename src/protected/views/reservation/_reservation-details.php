<?php
	/**
	 * This view renders reservation details in a modal.
	 *
	 * @param Reservation  $reservation  
	 */
	
	$id = $reservation->reservation_id;
	$status = $reservation->isOverdue() ? ReservationStatus::OVERDUE : $reservation->reservationStatus->reservation_status_name;
	$displays = $reservation->attributeDisplays();

	$attributes = array_merge(array(
		'reservation_status_id',
		'user_id',
		'beginning_date',
		'end_date',
		'item_id_list',
		'location_id'
	), array_keys($reservation->getFields()), array(
		'reservation_notes'
	));

	$colorSettings = Setting::model()
		->currentBranch()
		->type(SettingType::RESERVATION_STATUS_COLOR)
		->findAll();

	$resStatusColors = CHtml::listData($colorSettings, 'setting_attribute', 'setting_value');
?>

<div class="modal fade" id="reservation_details" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Reservation for <strong><?php echo $reservation->user->fullName; ?></strong></h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
					<?php
						foreach ($attributes as $name)
						{
							echo '<div class="row">';

							echo '<div class="col-xs-4">';
							echo '<p class="form-control-static control-label">';

							echo $reservation->getAttributeLabel($name);

							echo '</p>';
							echo '</div>';
							
							echo '<div class="col-xs-8">';
							echo '<p class="form-control-static">';

							if ($name == 'reservation_status_id')
							{
								echo '<strong style="color:' . $resStatusColors[ $status ] . '">';
								echo CHtml::encode($status);
								echo '</strong>';
							}
							else if ($name == 'item_id_list')
							{
								foreach ($reservation->reservationHasItems as $hasItem)
								{
									echo '<strong>' . $hasItem->item->itemType->item_type_name . '</strong>';
									echo ' ' . $hasItem->item->description . '<br />';
								}
							}
							else if (isset($displays[ $name ]))
							{
								echo CHtml::encode($displays[ $name ]);
							}
							else
							{
								echo CHtml::encode($reservation->$name);
							}

							echo '</p>';
							echo '</div>';

							echo '</div>';
						}	
					?>
				</div>
			</div>
			<div class="modal-footer">
				<div class="btn-group btn-group-justified">
					<?php
						// Agreement button
						// echo '<div class="btn-group">';
						// echo CHtml::link('Agreement', array(
						// 	'reservation/agreement',
						// 	'id' => $id
						// ), array(
						// 	'class' => 'btn btn-default',
						// 	'role' => 'button'
						// ));
						// echo '</div>';
						
						// Edit button
						echo '<div class="btn-group">';
						echo CHtml::link('Edit', array(
							'reservation/update',
							'id' => $id
						), array(
							'class' => 'btn btn-default',
							'role' => 'button'
						));
						echo '</div>';

						if ($status == ReservationStatus::PENDING || $status == ReservationStatus::READY)
						{
							// Cancel button
							echo '<div class="btn-group">';
							echo CHtml::link('Cancel', array(
								'reservation/cancel',
								'id' => $id
							), array(
								'class' => 'btn btn-danger',
								'role' => 'button'
							));
							echo '</div>';
						}

						if ($status == ReservationStatus::READY)
						{
							// Checkout button
							echo '<div class="btn-group">';
							echo CHtml::link('Checkout', array(
								'reservation/checkout',
								'id' => $id
							), array(
								'class' => 'btn btn-primary',
								'role' => 'button'
							));
							echo '</div>';
						}

						if ($status == ReservationStatus::ACTIVE || $status == ReservationStatus::OVERDUE)
						{
							// Close button
							echo '<div class="btn-group">';
							echo CHtml::link('Close', array(
								'reservation/close',
								'id' => $id
							), array(
								'class' => 'btn btn-primary',
								'role' => 'button'
							));
							echo '</div>';
						}
					?>
				</div>
			</div>

		</div>
	</div>
</div>