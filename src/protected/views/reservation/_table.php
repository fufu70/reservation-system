<?php
	/**
	 * @param CModel            $model    The model to use for the table.
	 * @param array<string>     $columns  The list of model attributes for the columns of the table.
	 * @param array<CModel>     $rows     The list of models for the rows of the table.
	 */
	
	$colorSettings = Setting::model()
		->currentBranch()
		->type(SettingType::RESERVATION_STATUS_COLOR)
		->findAll();

	$resStatusColors = CHtml::listData($colorSettings, 'setting_attribute', 'setting_value');
?>

<table class="table table-hover footable">
	<thead>
		<tr>
			<th data-sort-ignore="true"></th>
			<?php
				foreach ($columns as $name)
				{
					echo '<th class="fit">';
					echo CHtml::encode($model->getAttributeLabel($name));
					echo '</th>';
				}
			?>
			<th class="expand" data-sort-ignore="true"></th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($rows as $row)
			{
				echo '<tr>';

				// Start button group column
				echo '<td class="fit">';
				echo '<div class="btn-group" role="group">';

				$success = 'js:function(html) {
					$("#reservation_details").remove();
					$("body").children(".modal-backdrop").remove();
					$("body").append(html);
					processDynamicLabels();
					$("#reservation_details").modal("show");
				}';

				// Configure edit button
				$icon = '&nbsp;<span class="glyphicon glyphicon-eye-open"></span>&nbsp;';
				$htmlOptions = array(
					'title' => 'View',
					'class' => 'btn btn-xs btn-default',
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
				);

				echo CHtml::ajaxLink($icon,
					array('reservation/fetchDetails', 'id' => $row->primaryKey),
					array('success' => $success),
					$htmlOptions
				);
				
				$displays = $row->attributeDisplays();

				foreach ($columns as $name)
				{
					echo '<td class="fit">';

					if ($name == 'reservation_status_id')
					{
						$status = $row->isOverdue() ? ReservationStatus::OVERDUE : $row->reservationStatus->reservation_status_name;

						echo '<strong style="color:' . $resStatusColors[ $status ] . '">';
						echo CHtml::encode($status);
						echo '</strong>';
					}
					else
					{
						echo (isset($displays[ $name ])) ? CHtml::encode($displays[ $name ]) : CHtml::encode($row->$name);
					}

					echo '</td>';
				}

				// Expand column
				echo '<td></td>';

				echo '</tr>';
			}
		?>
	</tbody>
</table>
