<?php
	/**
	 * Displays a table of in-progress reservations, and forms for creating and updating reservations.
	 *
	 * @param CModel 			$model          The entity to create the forms for.
	 * @param array<CModel> 	$rows 	        The entity rows to display.
	 * @param String            $reservationID  The ID of a reservation to update. Optional
	 */
	
	$columns = array_merge(array(
		'reservation_status_id',
		'user_id',
		'beginning_date',
		'end_date',
		'location_id'
	), array_keys($model->getFields()));

	if (isset($reservationID))
	{
		// Render form for updating the reservation.
		$this->renderPartial('_reservation-wizard', array(
			'id' => 'update_reservation',
			'model' => Reservation::model()->findByPK($reservationID),
			'formConfig' => array(
				'action' => $this->createUrl('update'),
				'title' => 'Update Reservation',
			),
		));

		$js = '$("#update_reservation").modal("show");';
		Yii::app()->getClientScript()->registerScript('UpdateReservation', $js);
	}

	// Render form for creating reservations.
	$this->renderPartial('_reservation-wizard', array(
		'id' => 'create_reservation',
		'model' => new Reservation,
		'formConfig' => array(
			'action' => $this->createUrl('create'),
			'title' => 'Create Reservation',
		),
	));

	// Render form for creating field types.
	$this->renderPartial('/manage/_create-field-type', array(
		'id' => 'create_field_type',
		'entityType' => $model->entityType,
	));

	// Render form for updating field types.
	$this->renderPartial('/manage/_update-field-type', array(
		'id' => 'update_field_type',
		'entityType' => $model->entityType,
	));
?>

<div class="col-xs-12 form-group">
	<span class="h3">Reservations</span>
</div>
<div class="col-xs-12">
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group btn-group dynamic-label" role="group">
				<?php
					if (Yii::app()->user->hasAccess('reservation/create'))
					{
						echo CHtml::button('Create Reservation', array(
							'class' => 'btn btn-primary btn-sm',
							'data-target' => '#create_reservation',
							'data-toggle' => 'modal',
						));
					}

					if (Yii::app()->user->hasAccess('fieldType/create'))
					{
						echo CHtml::button('Create Field', array(
							'class' => 'btn btn-default btn-sm',
							'data-target' => '#create_field_type',
							'data-toggle' => 'modal',
						));
					}

					if (Yii::app()->user->hasAccess('fieldType/update'))
					{
						echo CHtml::button('Edit Field', array(
							'class' => 'btn btn-default btn-sm',
							'data-target' => '#update_field_type',
							'data-toggle' => 'modal',
						));
					}
				?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default scroll-x">
				<?php				
					$this->renderPartial('/reservation/_table', array(
						'model' => $model,
						'columns' => $columns,
						'rows' => $rows,
					));
				?>
			</div>
		</div>
	</div>
</div>
