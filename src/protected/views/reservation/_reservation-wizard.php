<?php
	/**
	 * [description]
	 *
	 * @param CModel    $model
	 * @param array     $formConfig
	 */

	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/site/reservation.js', CClientScript::POS_END);

	$inputTypes = $model->inputTypes();
	$hints = $model->hints();

	$detailElements = array();

	$detailElements['location_id'] = array(
		'type' => $inputTypes['location_id']
	);

	foreach ($model->getFields() as $field => $value)
	{
		$detailElements[ $field ] = array(
			'type' => $inputTypes[ $field ],
			'hint' => $hints[ $field ],
		);
	}

	$detailElements['reservation_notes'] = array(
		'type' => $inputTypes['reservation_notes']
	);

	$formConfig = array_merge($formConfig, array(
		'activeForm' => array(
			'class' => 'CActiveForm',
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => true,
				'validateOnType' => false,
			),
		),
		'elements' => array(
			'contact' => array(
				'type' => 'form',
				'elements' => array(
					'reservation_id' => array(
						'type' => $inputTypes['reservation_id']
					),
					'user_id' => array(
						'type' => $inputTypes['user_id']
					),
				),
			),
			'dateRange' => array(
				'type' => 'form',
				'elements' => array(
					'beginning_date' => array(
						'type' => $inputTypes['beginning_date']
					),
					'end_date' => array(
						'type' => $inputTypes['end_date']
					),
				),
			),
			'item' => array(
				'type' => 'form',
				'elements' => array(
					'item_id_list' => array(
						'type' => $inputTypes['item_id_list'],
						'multiple' => true
					),
				),
			),
			'details' => array(
				'type' => 'form',
				'elements' => $detailElements,
			),
		),
	));

	$form = new CForm($formConfig, $model);
?>

<div class="modal fade" id="<?php if (isset($id)) echo $id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<?php echo $form->renderBegin(); ?>

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?php echo $form->title; ?></h4>
			</div>
			<div class="wizard" data-initialize="wizard">
				<ul class="steps">
					<li data-step="1" class="active"><span class="badge">1</span>Contact<span class="chevron"></span></li>
					<li data-step="2"><span class="badge">2</span>Date Range<span class="chevron"></span></li>
					<li data-step="3" class="dynamic-label"><span class="badge">3</span>Item<span class="chevron"></span></li>
					<li data-step="4"><span class="badge">4</span>Details<span class="chevron"></span></li>
				</ul>
				<div class="actions">
					<button type="button" class="btn btn-default btn-prev"><span class="glyphicon glyphicon-arrow-left"></span>Prev</button>
					<button type="button" class="btn btn-default btn-next" data-last="Submit">Next<span class="glyphicon glyphicon-arrow-right"></span></button>
				</div>
				<div class="step-content">
					<div class="step-pane active" data-step="1">
						<h4>Select Contact Info</h4>
						<?php
							$this->renderPartial('/global/_two-column-elements', array(
								'elements' => $form->elements['contact']->elements,
							));
						?>
					</div>
					<div class="step-pane" data-step="2">
						<h4>Select Date Range</h4>
						<?php
							$this->renderPartial('/global/_two-column-elements', array(
								'elements' => $form->elements['dateRange']->elements,
							));
						?>
					</div>
					<div class="step-pane" data-step="3">
						<h4 class="dynamic-label">Select Items</h4>
						<?php
							$this->renderPartial('/global/_two-column-elements', array(
								'elements' => $form->elements['item']->elements,
							));
						?>
					</div>
					<div class="step-pane" data-step="4">
						<h4>Specify Details</h4>
						<?php
							$this->renderPartial('/global/_two-column-elements', array(
								'elements' => $form->elements['details']->elements,
							));
						?>
					</div>
				</div>
			</div>

			<?php echo $form->renderEnd(); ?>

		</div>
	</div>
</div>
