<?php
	$items = array();

	if (Yii::app()->user->hasAccess('reservation/create'))
	{
		$items[] = array(
			'label' => 'Create',
			'url' => array('reservation/create'),
		);
	}

	$this->widget('SidebarMenu', array('title' => 'Reservation', 'items' => $items));
?>
