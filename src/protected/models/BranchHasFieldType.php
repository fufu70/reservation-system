<?php

Yii::import('application.models._base.BaseBranchHasFieldType');

class BranchHasFieldType extends BaseBranchHasFieldType
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}