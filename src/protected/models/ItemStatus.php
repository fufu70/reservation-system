<?php

Yii::import('application.models._base.BaseItemStatus');

/**
 * The ItemStatus class is there to represent all of the information regarding the rows in the
 * Item Status table.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class ItemStatus extends BaseItemStatus
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = array(
			array('item_status_name', 'unique', 'criteria' => self::model()->currentBranch()->getDbCriteria()),
			array('available', 'boolean'),
		);
		return array_merge(parent::rules(), $rules);
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		$itemName = Setting::model()->type(SettingType::ITEM_NAME)->currentBranch()->find()->setting_value;
		
		$labels = array(
			'item_status_name' => $itemName . ' Status',
		);
		return array_merge(parent::attributeLabels(), $labels);
	}

	/**
	 * Declares the attribute input types.
	 */
	public function inputTypes()
	{
		$inputTypes = array(
			'item_status_id' => 'hidden',
			'item_status_name' => InputType::model()->getPathToWidget(InputType::TEXT),
			'available' => InputType::model()->getPathToWidget(InputType::CHECKBOX),
		);
		return array_merge($inputTypes, parent::inputTypes());
	}

	/**
	 * Declares the attribute hints.
	 */
	public function hints()
	{
		$hints = array(
			'available' => 'Whether an item with this status can be reserved.',
		);
		return array_merge(parent::hints(), $hints);
	}

	public function createFieldRelation()
	{
		return new ItemStatusHasField;
	}

	public function getFieldRelations()
	{
		return $this->itemStatusHasFields;
	}

	/**
		
		Events
																	   
	 */
	
	protected function beforeValidate()
	{
		$this->branch_id = Yii::app()->user->branch->branch_id;

		return parent::beforeValidate();
	}

	/**
		
		Scopes
																	   
	 */
	
	/**
	 * Declares all non-parameterized scopes.
	 * 
	 * @return array
	 */
	public function scopes()
	{
		return array(
			'available' => array(
				'condition' => 'available = 1',
			),
			'unavailable' => array(
				'condition' => 'available = 0',
			),
		);
	}
	
	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return ItemStatus    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}
	
	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 		$branchID 	The ID of the branch to filter by.
	 * @return ItemStatus           A reference to this.
	 */
	public function branch($branchID)
	{
	    $this->getDbCriteria()->mergeWith(array(
			'condition' => 't.branch_id = :branchID',
			'params' => array(':branchID' => $branchID),
		));
	    return $this;
    }
	
	/**
	 * Filters criteria by item status name.
	 * 
	 * @param  string	$name 	The name of the item status to filter by.
	 * @return ItemStatus 		A reference to this.
	 */
	public function name($name)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.item_status_name = :name',
			'params' => array(':name' => $name),
		));
		return $this;
	}

	/**
	 * Filters criteria by item statuses that have not been deleted.
	 * 
	 * @return ItemStatus       A reference to this.
	 */
	public function unarchived()
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 't.archived_date IS NULL',
	    ));
	    return $this;
	}
}
