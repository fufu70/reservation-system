<?php

/**
 * The FieldRelation interface describes the capability of a model to store a relation
 * between a field and a custom entity.
 *
 * @author   John Salis <jsalis@stetson.edu>, Christian Micklisch <cmicklis@stetson.edu>
 * @since 	 v2.0.0
 */
interface FieldRelation
{
	/**
	 * Sets the ID of the related entity.
	 * 
	 * @param int $id
	 */
	public function setEntityID($id);

	/**
	 * Gets the ID of the related entity.
	 * 
	 * @return int
	 */
	public function getEntityID();

	/**
	 * Gets a new instance of the model that represents the related custom entity.
	 * 
	 * @return CustomEntity
	 */
	public function getCustomEntity();
}
