<?php

Yii::import('application.models._base.BaseLocation');

/**
 * [Location description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class Location extends BaseLocation
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = array(
			array('location_name', 'unique', 'criteria' => self::model()->currentBranch()->getDbCriteria()),
		);
		return array_merge(parent::rules(), $rules);
	}

	/**
	 * Declares the attribute input types.
	 */
	public function inputTypes()
	{
		$inputTypes = array(
			'location_id' => 'hidden',
			'location_name' => InputType::model()->getPathToWidget(InputType::TEXT),
		);
		return array_merge($inputTypes, parent::inputTypes());
	}

	public function createFieldRelation()
	{
		return new LocationHasField;
	}

	public function getFieldRelations()
	{
		return $this->locationHasFields;
	}

	/**
	
		Events

	 */
	
	protected function beforeValidate()
	{
		$this->branch_id = Yii::app()->user->branch->branch_id;

		return parent::beforeValidate();
	}

	/**
	   
	   Scopes
																	   
	 */
	
	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return Location    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}
	
	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 		$branchID 	The ID of the branch to filter by.
	 * @return Location             A reference to this.
	 */
	public function branch($branchID)
	{
	    $this->getDbCriteria()->mergeWith(array(
			'condition' => 't.branch_id = :branchID',
			'params' => array(':branchID' => $branchID),
		));
	    return $this;
    }
	
	/**
	 * Filters criteria by location name.
	 * 
	 * @param  string	$name 	The name of the location to filter by.
	 * @return Location      	A reference to this.
	 */
	public function name($name)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.location_name = :name',
			'params' => array(':name' => $name),
		));
		return $this;
	}

	/**
	 * Filters criteria by item statuses that have not been deleted.
	 * 
	 * @return Location         A reference to this.
	 */
	public function unarchived()
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 't.archived_date IS NULL',
	    ));
	    return $this;
	}
}
