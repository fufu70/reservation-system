<?php

Yii::import('application.models._base.BaseAccess');

/**
 * The Access class represents an active record in the Access table. The purpose of the table 
 * is to state which user role has right to reach specific action in their respective system. 
 * This action is associated with a class name that is pulled before the action is called, if 
 * the user's current role is on that list of user roles then the action is displayed, otherwise 
 * it is not.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class Access extends BaseAccess
{	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * [updateFromList description]
	 * 
	 * @param array $list [description]
	 */
	public function updateFromList($list)
	{
		$accessList = Access::model()
			->currentBranch()
			->userRole(Yii::app()->user->role)
			->findAll();
			
		$userRoleList = UserRole::model()
			->currentBranch()
			->maxRank(Yii::app()->user->role)
			->findAll();

		foreach ($accessList as $access)
		{
			foreach ($userRoleList as $userRole)
			{
				if (isset($list[ $access->controller_name ][ $access->action_name ][ $userRole->user_role_name ]))
				{
					$enabled = $list[ $access->controller_name ][ $access->action_name ][ $userRole->user_role_name ];
					$userRole->updateAccess($access, $enabled);
				}
			}
		}
	}

	/**
	 * Gets the controller name as a displayable label.
	 * 
	 * @return string
	 */
	public function getControllerLabel()
	{
		return ucfirst(preg_replace('/([a-z])([A-Z])/s', '$1 $2', $this->controller_name));
	}

	/**
	 * Gets the action name as a displayable label.
	 * 
	 * @return string
	 */
	public function getActionLabel()
	{
		return ucfirst(preg_replace('/([a-z])([A-Z])/s', '$1 $2', $this->action_name));
	}

	/**
	 * Gets the route associated with the access.
	 * 
	 * @return string
	 */
	public function getRoute()
	{
		return $this->controller_name . '/' . $this->action_name;
	}

	/**
	   
	   Scopes
																	   
	 */
	
	/**
	 * Declares all non-parameterized scopes.
	 * 
	 * @return array
	 */
	public function scopes()
	{
		return array(
			'visible' => array(
				'condition' => 'visible = 1',
			),
			'invisible' => array(
				'condition' => 'visible = 0',
			),
		);
	}

	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return Access    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}

	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 		$branchID 	The ID of the branch to filter by.
	 * @return Access               A reference to this.
	 */
	public function branch($branchID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'with' => array('branchHasAccesses' => array('select' => false)),
			'condition' => 'branchHasAccesses.branch_id = :branchID',
			'params' => array(':branchID' => $branchID),
		));
		return $this;
	}
	
	/**
	 * Filters criteria by controller.
	 * 
	 * @param  string $controllerName 	The name of the controller to filter by.
	 * @return Access                 	A reference to this.
	 */
	public function controller($controllerName)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'controller_name = :controllerName',
			'params' => array(':controllerName' => $controllerName),
		));
		return $this;
	}

	/**
	 * Filters criteria by action.
	 * 
	 * @param  string 	$actionName 	The name of the action to filter by.
	 * @return Access                 	A reference to this.
	 */
	public function action($actionName)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'action_name = :actionName',
			'params' => array(':actionName' => $actionName),
		));
		return $this;
	}

	/**
	 * Filters criteria by user role.
	 * 
	 * @param  string 		$userRole 	The name of the user role to filter by.
	 * @return Access                 	A reference to this.
	 */
	public function userRole($userRole)
	{
		$this->getDbCriteria()->mergeWith(array(
			'with' => array('userRoleHasAccesses.userRole' => array('select' => false)),
			'condition' => 'userRole.user_role_name = :userRole',
			'params' => array(':userRole' => $userRole),
		));
		return $this;
	}
}
