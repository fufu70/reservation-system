<?php

Yii::import('application.models._base.BaseReservation');

/**
 * The Reservation class represernts a single reservation for one or more items during a 
 * specified time period. Items can only be reserved when they have not been previously 
 * reserved during the same time period. The user, location, and reservation status are 
 * also included.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class Reservation extends BaseReservation
{
	public $item_id_list = array();

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		$itemName = Setting::model()->type(SettingType::ITEM_NAME)->find()->setting_value;

		$labels = array(
			'reservation_status_id' => 'Reservation Status',
			'user_id' => 'Primary Contact',
			'location_id' => 'Location',
			'item_id_list' => $itemName . 's',
		);
		return array_merge(parent::attributeLabels(), $labels);
	}

	/**
	 * [attributeDisplays description]
	 * 
	 * @return array
	 */
	public function attributeDisplays()
	{
		return array(
			'reservation_status_id' => $this->reservationStatus->reservation_status_name,
			'user_id' => $this->user->email_address,
			'location_id' => $this->location->location_name,
		);
	}

	/**
	 * Declares the attribute input types.
	 */
	public function inputTypes()
	{
		$inputTypes = array(
			'reservation_id' => 'hidden',
			'user_id' => InputType::model()->getPathToWidget(InputType::USER),
			'item_id_list' => InputType::model()->getPathToWidget(InputType::ITEM),
			'beginning_date' => InputType::model()->getPathToWidget(InputType::DATE_TIME),
			'end_date' => InputType::model()->getPathToWidget(InputType::DATE_TIME),
			'location_id' => InputType::model()->getPathToWidget(InputType::LOCATION),
			'reservation_notes' => InputType::model()->getPathToWidget(InputType::TEXT_AREA),
		);
		return array_merge($inputTypes, parent::inputTypes());
	}

	public function createFieldRelation()
	{
		return new ReservationHasField;
	}

	public function getFieldRelations()
	{
		return $this->reservationHasFields;
	}

	public function checkout($reservationID)
	{
		$this->updateByPK($reservationID, array(
			'reservation_status_id' => ReservationStatus::model()->active()->find()->reservation_status_id
		));
	}

	public function close($reservationID)
	{
		$this->updateByPK($reservationID, array(
			'reservation_status_id' => ReservationStatus::model()->closed()->find()->reservation_status_id
		));
	}

	public function cancel($reservationID)
	{
		$this->updateByPK($reservationID, array(
			'reservation_status_id' => ReservationStatus::model()->canceled()->find()->reservation_status_id
		));
	}

	/**
	 * Returns a value indicating whether the reservation is overdue.
	 * 
	 * @return boolean
	 */
	public function isOverdue()
	{
		$format = Yii::app()->params->dbDateFormat;
		$end = DateTime::createFromFormat($format, $this->end_date);
		$now = new DateTime;

		$status = $this->reservationStatus->reservation_status_name;

		return ($status == ReservationStatus::ACTIVE) && ($now > $end);
	}

	/**
	
		Validation

	 */

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = array(
			array('end_date', 'compareDates'),
			array('end_date', 'lengthInRange'),
			array('item_id_list', 'required'),
			array('item_id_list', 'availableItems'),
		);
		return array_merge($rules, parent::rules());
	}

	/**
	 * Validator that checks if the end date is greater than the beginning date.
	 */
	public function compareDates()
	{
		if (strtotime($this->beginning_date) >= strtotime($this->end_date))
		{
			$this->addError('end_date', 'End Date must be greater than Beginning Date.');
		}
	}

	/**
	 * Validator that checks if the reservation length is between the minimum and maximum.
	 */
	public function lengthInRange()
	{
		$minLength = Setting::model()
			->currentBranch()
			->type(SettingType::RESERVATION_LENGTH)
			->attribute('min')
			->find()
			->setting_value;

		$maxLength = Setting::model()
			->currentBranch()
			->type(SettingType::RESERVATION_LENGTH)
			->attribute('max')
			->find()
			->setting_value;

		$length = (strtotime($this->end_date) - strtotime($this->beginning_date)) / 60;

		if ($length < $minLength)
		{
			$this->addError('end_date', 'Reservation length is too short.');
		}
		else if ($length > $maxLength)
		{
			$this->addError('end_date', 'Reservation length is too long.');
		}
	}

	/**
	 * Validator that checks availability of all reservation items.
	 */
	public function availableItems()
	{
		foreach ($this->item_id_list as $itemID)
		{
			$criteria = array();

			if (isset($this->reservation_id))
			{
				$criteria = array(
					'condition' => 't.reservation_id != :id',
					'params' => array(':id' => $this->reservation_id),
				);
			}

			$reservation = Reservation::model()
				->item($itemID)
				->dateRange($this->beginning_date, $this->end_date)
				->inProgress()
				->find($criteria);

			if (!empty($reservation))
			{
				$item = Item::model()->findByPK($itemID);

				$this->addError('item_id_list', "Item '{$item->description}' is not available during the specified period.");
			}
		}
	}

	/**
	
		Events
	
	 */
	
	protected function afterFind()
	{
		parent::afterFind();

		foreach ($this->reservationHasItems as $relation)
		{
			$this->item_id_list[] = $relation->item->item_id;
		}
	}
	
	protected function beforeValidate()
	{
		$this->branch_id = Yii::app()->user->branch->branch_id;

		// Set the status of new reservations to ready.
		if ($this->isNewRecord)
		{
			$this->reservation_status_id = ReservationStatus::model()->ready()->find()->reservation_status_id;
		}

		return parent::beforeValidate();
	}

	public function beforeSave()
	{
		if (parent::beforeSave())
		{
			// Format the begining and end dates.
			$format = Yii::app()->params->dbDateFormat;
			$this->beginning_date = (new DateTime($this->beginning_date))->format($format);
			$this->end_date = (new DateTime($this->end_date))->format($format);

			return true;
		}
		else
			return false;
	}

	protected function afterSave()
	{
		parent::afterSave();

		$this->saveItems();
	}

	private function saveItems()
	{
		$currentItems = array();

		foreach ($this->reservationHasItems as $relation)
		{
			if (!in_array($relation->item_id, $this->item_id_list))
			{
				$relation->delete();
			}
			else
			{
				$currentItems[] = $relation->item_id;
			}
		}

		foreach (array_diff($this->item_id_list, $currentItems) as $itemID)
		{
			$relation = new ReservationHasItem;
			$relation->reservation_id = $this->reservation_id;
			$relation->item_id = $itemID;
			$relation->save();
		}
	}

	/**
		
		Scopes

	 */
	
	/**
	 * Declares all non-parameterized scopes.
	 * 
	 * @return array
	 */
	public function scopes()
	{
		return array(
			'pending' => array(
				'with' => array('reservationStatus' => array(
					'condition' => 'reservationStatus.reservation_status_name = :pending',
					'params' => array(':pending' => ReservationStatus::PENDING),
				)),
			),
			'ready' => array(
				'with' => array('reservationStatus' => array(
					'condition' => 'reservationStatus.reservation_status_name = :ready',
					'params' => array(':ready' => ReservationStatus::READY),
				)),
			),
			'active' => array(
				'with' => array('reservationStatus' => array(
					'condition' => 'reservationStatus.reservation_status_name = :active',
					'params' => array(':active' => ReservationStatus::ACTIVE),
				)),
			),
			'canceled' => array(
				'with' => array('reservationStatus' => array(
					'condition' => 'reservationStatus.reservation_status_name = :canceled',
					'params' => array(':canceled' => ReservationStatus::CANCELED),
				)),
			),
			'closed' => array(
				'with' => array('reservationStatus' => array(
					'condition' => 'reservationStatus.reservation_status_name = :closed',
					'params' => array(':closed' => ReservationStatus::CLOSED),
				)),
			),
			'inProgress' => array(
				'with' => array('reservationStatus' => array(
					'condition' => 'reservationStatus.reservation_status_name != :canceled && reservationStatus.reservation_status_name != :closed',
					'params' => array(':canceled' => ReservationStatus::CANCELED, ':closed' => ReservationStatus::CLOSED),
				)),
			),
		);
	}

	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return Reservation    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}
	
	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int          $branchID   The ID of the branch to filter by.
	 * @return Reservation              A reference to this.
	 */
	public function branch($branchID)
	{
		$this->getDbCriteria()->compare('t.branch_id', $branchID);
		return $this;
	}

	/**
	 * Filters criteria by reservation status.
	 *
	 * @param  boolean      $return   Whether the criteria should be returned instead of merged.
	 * @return Reservation            A reference to this.
	 */
	public function status($statusName, $return = false)
	{
		$statusCriteria = new CDbCriteria;

		if ($statusName == ReservationStatus::OVERDUE)
		{
			$statusName = ReservationStatus::ACTIVE;
			$now = (new DateTime)->format(Yii::app()->params->dbDateFormat);
			$statusCriteria->compare('end_date', '<' . $now);
		}

		$statusCriteria->compare('reservationStatus.reservation_status_name', $statusName);

		$criteria = new CDbCriteria;
		$criteria->with = array('reservationStatus' => array(
			'condition' => $statusCriteria->condition,
			'params' => $statusCriteria->params
		));

		if ($return)
		{
			return $criteria;
		}
		else
		{
			$this->getDbCriteria()->mergeWith($criteria);
			return $this;
		}
	}

	/**
	 * Filters criteria by item.
	 * 
	 * @param  int 	        $itemID      The ID of the item to filter by.
	 * @param  boolean      $return      Whether the criteria should be returned instead of merged.
	 * @return Reservation               A reference to this.
	 */
	public function item($itemID, $return = false)
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('reservationHasItems' => array('select' => false));
		$criteria->compare('reservationHasItems.item_id', $itemID);

		if ($return)
		{
			return $criteria;
		}
		else
		{
			$this->getDbCriteria()->mergeWith($criteria);
			return $this;
		}
	}

	/**
	 * Filters criteria by item type.
	 * 
	 * @param  int 	        $itemTypeID  The ID of the item type to filter by.
	 * @param  boolean      $return      Whether the criteria should be returned instead of merged.
	 * @return Reservation               A reference to this.
	 */
	public function itemType($itemTypeID, $return = false)
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('reservationHasItems.item' => array('select' => false));
		$criteria->compare('item.item_type_id', $itemTypeID);

		if ($return)
		{
			return $criteria;
		}
		else
		{
			$this->getDbCriteria()->mergeWith($criteria);
			return $this;
		}
	}

	/**
	 * Filters criteria by item status.
	 * 
	 * @param  int 	        $itemStatusID  The ID of the item status to filter by.
	 * @param  boolean      $return        Whether the criteria should be returned instead of merged.
	 * @return Reservation                 A reference to this.
	 */
	public function itemStatus($itemStatusID, $return = false)
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('reservationHasItems.item' => array('select' => false));
		$criteria->compare('item.item_status_id', $itemStatusID);

		if ($return)
		{
			return $criteria;
		}
		else
		{
			$this->getDbCriteria()->mergeWith($criteria);
			return $this;
		}
	}

	/**
	 * Filters criteria by date range. Reservations that partially overlap with the 
	 * date range are included.
	 * 
	 * @param  string       $begin    The beginning date to filter by.
	 * @param  string       $end      The end date to filter by.
	 * @param  boolean      $return   Whether the criteria should be returned instead of merged.
	 * @return Reservation            A reference to this.
	 */
	public function dateRange($begin, $end, $return = false)
	{
		$criteria = new CDbCriteria;
		$format = Yii::app()->params->dbDateFormat;

		if (!empty($begin))
		{
			$begin = (new DateTime($begin))->format($format);
			$criteria->compare('t.end_date', '>' . $begin);
		}

		if (!empty($end))
		{
			$end = (new DateTime($end))->format($format);
			$criteria->compare('t.beginning_date', '<' . $end);
		}

		if ($return)
		{
			return $criteria;
		}
		else
		{
			$this->getDbCriteria()->mergeWith($criteria);
			return $this;
		}		
	}
}
