<?php

Yii::import('application.models._base.BaseItemStatusHasField');
Yii::import('application.models._interfaces.FieldRelation');

class ItemStatusHasField extends BaseItemStatusHasField implements FieldRelation
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getCustomEntity()
	{
		return new ItemStatus;
	}

	public function getEntityID()
	{
		return $this->item_status_id;
	}

	public function setEntityID($id)
	{
		$this->item_status_id = $id;
	}
}