<?php

/**
 * The DoorKeeperForm class handles validation of login and signup.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class DoorKeeperForm extends CFormModel
{
	public $remember_me = false;
	public $email_address;
	public $password;
	public $password_repeat;
	public $first_name;
	public $last_name;

	private $_identity;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('email_address, password', 'required', 'on' => 'login, signup'),
			array('password', 'authenticate', 'on' => 'login'),
			array('first_name, last_name, password_repeat', 'required', 'on' => 'signup'),
			array('password', 'compare', 'compareAttribute' => 'password_repeat', 'on' => 'signup'),
			array('email_address', 'unique', 'className' => 'User', 'attributeName' => 'email_address', 'on' => 'signup'),
			array('email_address', 'email'),
			array('remember_me', 'boolean'),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in the rules.
	 */
	public function authenticate()
	{
		$this->_identity = new UserIdentity($this->email_address, $this->password);

		if (!$this->_identity->authenticate())
		{
            $this->addError('password', 'Incorrect email or password.');
        }
	}

	/**
	 * If the form passes validation, an appropriate action is taken based on the scenario.
	 * 
	 * @return boolean 	Whether form submit was successful.
	 */
	public function submit()
	{
		if ($this->validate())
		{
			if ($this->scenario == 'login')
			{
				$duration = ($this->remember_me) ? Yii::app()->params->loginDuration : 0;
				Yii::app()->user->login($this->_identity, $duration);
			}
			else if ($this->scenario == 'signup')
			{
				$user = new User;
				$user->attributes = $this->attributes;
				$user->password = $this->password;
				$user->save();

				$this->addErrors($user->getErrors());
			}
			else
			{
				throw new CException(Yii::t('yii', "The scenario '{$this->scenario}' is invalid."));
			}
		}
		return !$this->hasErrors();
	}
}
