<?php

/**
 * The ChangePasswordForm class handles validation of password changes.
 * 
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class ChangePasswordForm extends CFormModel
{
	public $current_password;
	public $new_password;
	public $confirm_password;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('current_password, new_password, confirm_password', 'required'),
			array('current_password', 'authenticate'),
			array('confirm_password', 'compare', 'compareAttribute' => 'new_password'),
		);
	}

	/**
	 * This validator authenticates the current password.
	 */
	public function authenticate()
	{
		if (!Login::authenticate(Yii::app()->user->id, $this->current_password))
		{
            $this->addError('current_password', 'Incorrect password.');
        }
	}
	
	/**
	 * If the form passes validation, the user password is updated.
	 * 
	 * @return boolean 	Whether form submit was successful.
	 */
	public function submit()
	{
		if ($this->validate())
		{
			Login::model()->updateByPk(Yii::app()->user->id, array(
				'password' => Login::createHash($this->new_password)
			));

			return true;
		}
		else
			return false;
	}
}
