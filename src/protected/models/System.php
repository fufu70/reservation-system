<?php

Yii::import('application.models._base.BaseSystem');

/**
 * [System description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class System extends BaseSystem
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	
		Scopes

	 */
	
	/**
	 * Filters criteria by system name.
	 * 
	 * @param  string 	$systemName 	The name of the system to filter by.
	 * @return System                 	A reference to this.
	 */
	public function name($systemName)
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 'system_name = :systemName',
            'params' => array(':systemName' => $systemName),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 		$branchID 	The ID of the branch to filter by.
	 * @return System               A reference to this.
	 */
	public function branch($branchID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('branches' => array('select' => false)),
	        'condition' => 'branches.branch_id = :branchID',
            'params' => array(':branchID' => $branchID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by user.
	 * 
	 * @param  int 		$userID 	The ID of the user to filter by.
	 * @return System               A reference to this.
	 */
	public function user($userID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('systemHasUsers' => array('select' => false)),
	        'condition' => 'systemHasUsers.user_id = :userID',
            'params' => array(':userID' => $userID),
	    ));
	    return $this;
	}
}
