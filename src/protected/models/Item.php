<?php

Yii::import('application.models._base.BaseItem');

/**
 * The Item class is there to represent all of the Items in a Set of Systems.
 * Each row is a unique identifier of a different and unique item, and each item is 
 * related to a system. An Item consists of a accession date, deaccession date, a
 * status, type, location, and a description.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class Item extends BaseItem
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		$itemName = Setting::model()->type(SettingType::ITEM_NAME)->currentBranch()->find()->setting_value;

		$labels = array(
			'item_type_id' => $itemName . ' Type',
			'item_status_id' => $itemName . ' Status',
		);
		return array_merge(parent::attributeLabels(), $labels);
	}

	/**
	 * [attributeDisplays description]
	 * 
	 * @return array
	 */
	public function attributeDisplays()
	{
		return array(
			'item_type_id' => $this->itemType->item_type_name,
			'item_status_id' => $this->itemStatus->item_status_name,
		);
	}

	/**
	 * Declares the attribute input types.
	 */
	public function inputTypes()
	{
		$inputTypes = array(
			'item_id' => 'hidden',
			'item_type_id' => InputType::model()->getPathToWidget(InputType::ITEM_TYPE),
			'item_status_id' => InputType::model()->getPathToWidget(InputType::ITEM_STATUS),
			'description' => InputType::model()->getPathToWidget(InputType::TEXT),
		);
		return array_merge($inputTypes, parent::inputTypes());
	}

	public function createFieldRelation()
	{
		return new ItemHasField;
	}

	public function getFieldRelations()
	{
		return $this->itemHasFields;
	}

	/**
	
		Events
	
	 */
	
	protected function beforeValidate()
	{
		$this->branch_id = Yii::app()->user->branch->branch_id;

		return parent::beforeValidate();
	}

	/**
	
		Scopes

	 */
	
	/**
	 * Declares all non-parameterized scopes.
	 * 
	 * @return array
	 */
	public function scopes()
	{
		return array(
			'available' => array(
				'with' => array('itemStatus' => array(
					'condition' => 'itemStatus.available = 1',
				)),
			),
			'unavailable' => array(
				'with' => array('itemStatus' => array(
					'condition' => 'itemStatus.available = 0',
				)),
			),
		);
	}
	
	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return Item    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}
	
	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 		$branchID 	The ID of the branch to filter by.
	 * @return Item                 A reference to this.
	 */
	public function branch($branchID)
	{
		$this->getDbCriteria()->compare('t.branch_id', $branchID);
		return $this;
	}

	/**
	 * Filters criteria by item status.
	 * 
	 * @param  int 		$itemStatusID 	The ID of the item status to filter by.
	 * @return Item                 	A reference to this.
	 */
	public function status($itemStatusID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.item_status_id = :itemStatusID',
			'params' => array(':itemStatusID' => $itemStatusID),
		));
		return $this;
	}

	/**
	 * Filters criteria by item type.
	 * 
	 * @param  int 		$itemTypeID 	The ID of the item type to filter by.
	 * @return Item                 	A reference to this.
	 */
	public function type($itemTypeID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.item_type_id = :itemTypeID',
			'params' => array(':itemTypeID' => $itemTypeID),
		));
		return $this;
	}

	/**
	 * Filters criteria by items that have not been deleted.
	 * 
	 * @return Item             A reference to this.
	 */
	public function unarchived()
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 't.archived_date IS NULL',
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by items reserved during a date range.
	 * 
	 * @param  string 	$begin   The beginning date to filter by.
	 * @param  string 	$end     The end date to filter by.
	 * @param  boolean  $return  Whether the criteria should be returned instead of merged.
	 * @return Item              A reference to this.
	 */
	public function reserved($begin, $end, $return = false)
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('reservationHasItems.reservation:inProgress' => array('select' => false));
		$format = Yii::app()->params->dbDateFormat;

		if (!empty($begin))
		{
			$begin = (new DateTime($begin))->format($format);
			$criteria->compare('reservation.end_date', '>' . $begin);
		}

		if (!empty($end))
		{
			$end = (new DateTime($end))->format($format);
			$criteria->compare('reservation.beginning_date', '<' . $end);
		}

		if ($return)
		{
			return $criteria;
		}
		else
		{
			$this->getDbCriteria()->mergeWith($criteria);
			return $this;
		}
	}

	/**
	 * Filters criteria by items unreserved during a date range.
	 * 
	 * @param  string 	$begin   The beginning date to filter by.
	 * @param  string 	$end     The end date to filter by.
	 * @param  boolean  $return  Whether the criteria should be returned instead of merged.
	 * @return Item              A reference to this.
	 */
	public function unreserved($begin, $end, $return = false)
	{
		$newCriteria = new CDbCriteria;
		$format = Yii::app()->params->dbDateFormat;

		// Issue with query: condition must apply to all related reservations.

		if (!empty($begin))
		{
			$begin = (new DateTime($begin))->format($format);
			$newCriteria->compare('reservation.end_date', '>' . $begin);
		}

		if (!empty($end))
		{
			$end = (new DateTime($end))->format($format);
			$newCriteria->compare('reservation.beginning_date', '<' . $end);
		}

		$criteria = new CDbCriteria(array(
			'with' => array(
				'reservationHasItems.reservation' => array(
					'select' => false,
					'joinType' => 'LEFT JOIN',
					'on' => $newCriteria->condition,
					'params' => $newCriteria->params,
				),
			),
		));

		$criteria->condition = 'reservation.reservation_id IS NULL';

		if ($return)
		{
			return $criteria;
		}
		else
		{
			$this->getDbCriteria()->mergeWith($criteria);
			return $this;
		}
	}
}
