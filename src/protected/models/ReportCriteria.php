<?php

Yii::import('application.models._base.BaseReportCriteria');

class ReportCriteria extends BaseReportCriteria
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}