<?php

Yii::import('application.models._base.BaseLocationHasField');
Yii::import('application.models._interfaces.FieldRelation');

class LocationHasField extends BaseLocationHasField implements FieldRelation
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getCustomEntity()
	{
		return new Location;
	}

	public function getEntityID()
	{
		return $this->location_id;
	}

	public function setEntityID($id)
	{
		$this->location_id = $id;
	}
}