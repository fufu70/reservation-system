<?php

Yii::import('application.models._base.BaseUserHasField');
Yii::import('application.models._interfaces.FieldRelation');

class UserHasField extends BaseUserHasField implements FieldRelation
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getCustomEntity()
	{
		return new User;
	}

	public function getEntityID()
	{
		return $this->user_id;
	}

	public function setEntityID($id)
	{
		$this->user_id = $id;
	}
}