<?php

Yii::import('application.models._base.BaseReservationStatus');

class ReservationStatus extends BaseReservationStatus
{
	const PENDING = 'Pending';
	const READY = 'Ready';
	const ACTIVE = 'Active';
	const OVERDUE = 'Overdue';
	const CLOSED = 'Closed';
	const CANCELED = 'Canceled';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
		
		Scopes
																	   
	 */
	
	/**
	 * Declares all non-parameterized scopes.
	 * 
	 * @return array
	 */
	public function scopes()
	{
		return array(
			'pending' => array(
				'condition' => 't.reservation_status_name = :pending',
				'params' => array(':pending' => ReservationStatus::PENDING),
			),
			'ready' => array(
				'condition' => 't.reservation_status_name = :ready',
				'params' => array(':ready' => ReservationStatus::READY),
			),
			'active' => array(
				'condition' => 't.reservation_status_name = :active',
				'params' => array(':active' => ReservationStatus::ACTIVE),
			),
			'canceled' => array(
				'condition' => 't.reservation_status_name = :canceled',
				'params' => array(':canceled' => ReservationStatus::CANCELED),
			),
			'closed' => array(
				'condition' => 't.reservation_status_name = :closed',
				'params' => array(':closed' => ReservationStatus::CLOSED),
			),
		);
	}
}
