<?php

Yii::import('application.models._base.BaseBranchHasUserRole');

class BranchHasUserRole extends BaseBranchHasUserRole
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}