<?php

Yii::import('application.models._base.BaseLog');

class Log extends BaseLog
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    public function defaultScope()
	{
		return array(
	        'condition' => 't.branch_id = :branchID',
            'params' => array(':branchID' => Yii::app()->user->branch->branch_id),
	    );
    }

	/**
	   
	   Scopes
																	   
	 */
	
	/**
	 * Filters criteria by user.
	 * 
	 * @param  int 		$userID 	The ID of the user to filter by.
	 * @return Log                 	A reference to this.
	 */
	public function user($userID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'user_id = :userID',
			'params' => array(':userID' => $userID),
		));
		return $this;
	}

	/**
	 * Filters criteria by entity type.
	 * 
	 * @param  int 	$entityTypeID 	The ID of the entity type to filter by.
	 * @return Log                 	A reference to this.
	 */
	public function entityType($entityTypeID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'with' => 'attributeType.entityType',
			'condition' => 'entityType.entity_type_id = :entityTypeID',
			'params' => array(':entityTypeID' => $entityTypeID),
		));
		return $this;
	}

	/**
	 * Filters criteria by attribute type.
	 * 
	 * @param  int 	$attributeTypeID 	The ID of the attribute type to filter by.
	 * @return Log                 		A reference to this.
	 */
	public function attributeType($attributeTypeID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'with' => 'attributeType',
			'condition' => 'attributeType.attribute_type_id = :attributeTypeID',
			'params' => array(':attributeTypeID' => $attributeTypeID),
		));
		return $this;
	}
}
