<?php

Yii::import('application.models._base.BaseSystemHasUser');

class SystemHasUser extends BaseSystemHasUser
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
