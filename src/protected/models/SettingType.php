<?php

Yii::import('application.models._base.BaseSettingType');

class SettingType extends BaseSettingType
{
	const ITEM_NAME = 'Item Name';
	const TIME_ZONE = 'Time Zone';
	const RESERVATION_STATUS_COLOR = 'Reservation Status Color';
	const RESERVATION_LENGTH = 'Reservation Length';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}