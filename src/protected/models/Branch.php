<?php

Yii::import('application.models._base.BaseBranch');

class Branch extends BaseBranch
{
	const BASIC_BRANCH_ID = 1;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	
		Validation

	 */
	
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = array(
			// TO DO
		);
		return array_merge(parent::rules(), $rules);
	}

	/**
	
		Events
	
	 */

	protected function afterSave()
	{
		parent::afterSave();

		if ($this->isNewRecord)
		{
			$this->copySettings(Branch::BASIC_BRANCH_ID);
			$this->copyUserRoles(Branch::BASIC_BRANCH_ID);
			$this->copyAccess(Branch::BASIC_BRANCH_ID);

			$userRole = UserRole::model()->branch($this->branch_id)->find(array('order' => 'user_role_rank'));

			$relation = new BranchHasUser;
			$relation->branch_id = $this->branch_id;
			$relation->user_id = Yii::app()->user->id;
			$relation->user_role_id = $userRole->user_role_id;
			$relation->save();
		}
	}

	private function copySettings($branchID)
	{
		$settings = Setting::model()->branch($branchID)->findAll();

		foreach ($settings as $setting)
		{
			$copy = new Setting;
			$copy->attributes = $setting->attributes;
			$copy->branch_id = $this->branch_id;
			$copy->save();
		}
	}

	private function copyUserRoles($branchID)
	{
		$userRoles = UserRole::model()->branch($branchID)->findAll();

		foreach ($userRoles as $userRole)
		{
			$copy = new UserRole;
			$copy->attributes = $userRole->attributes;
			$copy->save();

			$relation = new BranchHasUserRole;
			$relation->user_role_id = $copy->user_role_id;
			$relation->branch_id = $this->branch_id;
			$relation->save();

			foreach ($userRole->userRoleHasAccesses as $hasAccess)
			{
				$relation = new UserRoleHasAccess;
				$relation->user_role_id = $copy->user_role_id;
				$relation->access_id = $hasAccess->access_id;
				$relation->save();
			}
		}
	}

	private function copyAccess($branchID)
	{
		$accessList = Access::model()->branch($branchID)->findAll();

		foreach ($accessList as $access)
		{
			$relation = new BranchHasAccess;
			$relation->access_id = $access->access_id;
			$relation->branch_id = $this->branch_id;
			$relation->save();
		}
	}

	/**
		
		Scopes

	 */

	/**
	 * Filters criteria by system.
	 * 
	 * @param  int 	$systemID 	The ID of the system to filter by.
	 * @return Branch           A reference to this.
	 */
	public function system($systemID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 't.system_id = :systemID',
            'params' => array(':systemID' => $systemID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by system user.
	 * 
	 * @param  int 	$userID 	The ID of the user to filter by.
	 * @return Branch           A reference to this.
	 */
	public function systemUser($userID)
	{
	    $this->getDbCriteria()->mergeWith(array(
			'with' => array('system.systemHasUsers' => array('select' => false)),
	        'condition' => 'systemHasUsers.user_id = :userID',
            'params' => array(':userID' => $userID),
	    ));
	    return $this;
	}
}
