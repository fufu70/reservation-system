<?php

Yii::import('application.models._base.BaseEntityType');

/**
 * The EntityType class represents all of the Entity Types in all of the
 * systems. The EntityType is there to represents all of atomic sets of data known
 * as rows. Each row on a table is defined by the table itself and the primary key.
 * The table name states the type of the entity hence the Entity Type class represents
 * what table the row is on.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class EntityType extends BaseEntityType
{
	const ACCESS = 'Access';
	const ATTRIBUTE = 'Attribute';
	const ATTRIBUTE_TYPE = 'Attribute Type';
	const BRANCH = 'Branch';
	const ENTITY_TYPE = 'Entity Type';
	const INPUT_TYPE = 'Input Type';
	const ITEM = 'Item';
	const ITEM_STATUS = 'Item Status';
	const ITEM_TYPE = 'Item Type';
	const LOCATION = 'Location';
	const LOG = 'Log';
	const LOGIN = 'Login';
	const MODULE = 'Module';
	const RESERVATION = 'Reservation';
	const RESERVATION_STATUS = 'Reservation Status';
	const SETTING = 'Setting';
	const SETTING_TYPE = 'Setting Type';
	const SYSTEM = 'System';
	const USER = 'User';
	const USER_ROLE = 'User Role';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
		
		Scopes
		                                                               
	 */

	/**
	 * Filters criteria by entity type name.
	 * 
	 * @param  string 	$name		The name of the entity type to filter by.
	 * @return EntityType   		A reference to this.
	 */
	public function name($name)
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 'entity_type_name = :name',
            'params' => array(':name' => $name),
	    ));
	    return $this;
	}
}
