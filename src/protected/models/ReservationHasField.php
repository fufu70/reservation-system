<?php

Yii::import('application.models._base.BaseReservationHasField');
Yii::import('application.models._interfaces.FieldRelation');

class ReservationHasField extends BaseReservationHasField implements FieldRelation
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getCustomEntity()
	{
		return new Reservation;
	}

	public function getEntityID()
	{
		return $this->reservation_id;
	}

	public function setEntityID($id)
	{
		$this->reservation_id = $id;
	}
}