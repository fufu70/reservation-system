<?php

Yii::import('application.models._base.BaseReport');

/**
 * [Report description]
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class Report extends BaseReport
{
	const SCOPE_IDENTIFIER = '#';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**

		Query Builder

	 */

	/**
	 * [buildQuery description]
	 * 
	 * @param  [type] $model [description]
	 * @param  [type] $input [description]
	 */
	public function buildQuery($model, $input)
	{
		$fieldCriteria = new CDbCriteria;

		// Iterate through each attribute.
		foreach ($input as $attribute => $criteria)
		{
			$isScope = ($attribute[0] == Report::SCOPE_IDENTIFIER);
			$isRange = !isset($criteria['value']);
			$isCustom = !array_key_exists($attribute, $model->attributes);

			$attrCriteria = new CDbCriteria;

			if ($isRange)
			{
				foreach ($criteria['from'] as $index => $from)
				{
					$to = $criteria['to'][$index];

					if ($isScope)
					{
						$scope = substr($attribute, 1);
						$attrCriteria->mergeWith($model->$scope($from, $to, true), 'OR');
					}
					else if ($isCustom)
					{
						$model->filterField($attribute, array($from, $to), $fieldCriteria);
					}
					else
					{
						$attrCriteria->addBetweenCondition($attribute, $from, $to, 'OR');
					}
				}
			}
			else
			{
				foreach ($criteria['value'] as $index => $value)
				{
					if ($isScope)
					{
						$scope = substr($attribute, 1);
						$attrCriteria->mergeWith($model->$scope($value, true), 'OR');
					}
					else if ($isCustom)
					{
						$model->filterField($attribute, $value, $fieldCriteria);
					}
					else
					{
						$attrCriteria->compare($attribute, $value, false, 'OR');
					}
				}
			}

			$model->getDbCriteria()->mergeWith($attrCriteria);
		}

		$model->getDbCriteria()->mergeWith($fieldCriteria);
	}
}
