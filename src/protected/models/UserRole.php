<?php

Yii::import('application.models._base.BaseUserRole');

/**
 * The UserRole class is there to represent everything with the UserRole table
 * in the database.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class UserRole extends BaseUserRole
{
	const SUPER = 'Super';
	const ADMIN = 'Admin';
	const BASIC = 'Basic';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Updates the user role relation to the specified access.
	 * 
	 * @param Access   $access    The related access.
	 * @param boolean  $enabled   Whether the user role has access.
	 */
	public function updateAccess($access, $enabled)
	{
		$exists = Access::model()
			->currentBranch()
			->controller($access->controller_name)
			->action($access->action_name)
			->userRole($this->user_role_name)
			->exists();

		if ($enabled && !$exists)
		{
			$relation = new UserRoleHasAccess;
			$relation->access_id = $access->access_id;
			$relation->user_role_id = $this->user_role_id;
			$relation->save();
		}
		else if (!$enabled && $exists)
		{
			UserRoleHasAccess::model()->deleteAllByAttributes(array(
				'user_role_id' => $this->user_role_id,
				'access_id' => $access->access_id
			));
		}
	}

	/**
		
		Scopes
																	   
	 */
	
	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return UserRole         A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}
	
	/**
	 * Filters criteria by user role name.
	 * 
	 * @param  string 	$roleName 	The name of the user role to filter by.
	 * @return UserRole         	A reference to this.
	 */
	public function name($roleName)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'user_role_name = :roleName',
			'params' => array(':roleName' => $roleName),
		));
		return $this;
	}

	/**
	 * Filters criteria by the rank of a user role. All user roles ranked below the 
	 * one specified are included.
	 * 
	 * @param  string 	$roleName 	The name of the user role to filter by.
	 * @return UserRole         	A reference to this.
	 */
	public function maxRank($roleName)
	{
		$rank = self::model()->name($roleName)->find()->user_role_rank; // Issue with separate query ??
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'user_role_rank > :rank',
			'params' => array(':rank' => $rank),
		));
		return $this;
	}

	/**
	 * Filters criteria by system.
	 * 
	 * @param  int 	$systemID 	The ID of the system to filter by.
	 * @return UserRole         A reference to this.
	 */
	public function system($systemID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'with' => array('systemHasUsers' => array('select' => false)),
			'condition' => 'systemHasUsers.system_id = :systemID',
			'params' => array(':systemID' => $systemID),
		));
		return $this;
	}

	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 	$branchID 	The ID of the branch to filter by.
	 * @return UserRole         A reference to this.
	 */
	public function branch($branchID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'with' => array('branchHasUserRoles' => array('select' => false)),
			'condition' => 'branchHasUserRoles.branch_id = :branchID',
			'params' => array(':branchID' => $branchID),
		));
		return $this;
	}

	/**
	 * Filters criteria by user.
	 * 
	 * @param  int 	$userID 	The ID of the user to filter by.
	 * @return UserRole         A reference to this.
	 */
	public function user($userID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'with' => array(
				'systemHasUsers' => array('select' => false),
				'branchHasUsers' => array('select' => false),
			),
			'condition' => '(systemHasUsers.user_id = :userID || branchHasUsers.user_id = :userID)',
			'params' => array(':userID' => $userID),
		));
		return $this;
	}
}
