<?php

Yii::import('application.models._base.BaseBranchHasModule');

class BranchHasModule extends BaseBranchHasModule
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}