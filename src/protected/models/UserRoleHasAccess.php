<?php

Yii::import('application.models._base.BaseUserRoleHasAccess');

class UserRoleHasAccess extends BaseUserRoleHasAccess
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
