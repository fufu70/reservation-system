<?php

/**
 * The CustomEntity class describes an entity that has the capability of storing
 * custom field values. All custom fields must be merged with the built-in fields of 
 * the entity defined by the attributes of the model.
 *
 * @author   John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
abstract class CustomEntity extends GxActiveRecord
{
	public $_fieldValues = array();
	public $_fieldTypes = array();

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Gets a new instance of the model that represents a relation between the entity and a field.
	 * 
	 * @return FieldRelation
	 */
	abstract public function createFieldRelation();

	/**
	 * Gets a list of the current field relations to the entity.
	 * 
	 * @return array<FieldRelation>
	 */
	abstract public function getFieldRelations();

	public function attributeDisplays() {} // TO DO : Declare this in ActiveRecord ??

	/**
	 * Initializes the custom field types associated with the entity.
	 */
	public function init()
	{
		if (Yii::app()->user->isBranchSelected())
		{
			// TO DO : This query might be better stored in the session.
			$this->_fieldTypes = FieldType::model()->currentBranch()->entityType($this->entityType)->findAll();

			// Initialize the field types associated with the entity.
			foreach ($this->_fieldTypes as $fieldType)
			{
				$id = $fieldType->identifier;
				$this->_fieldValues[ $id ] = '';
			}
		}
	}

	/**
	 * Declares the input type for each field type.
	 */
	public function inputTypes()
	{
		$inputTypes = array();

		foreach ($this->_fieldTypes as $fieldType)
		{
			$id = $fieldType->identifier;
			$inputTypes[ $id ] = $fieldType->inputType->pathToWidget;
		}
		return $inputTypes;
	}

	/**
	 * Declares the hints for each field type.
	 */
	public function hints()
	{
		$hints = array();

		foreach ($this->_fieldTypes as $fieldType)
		{
			$id = $fieldType->identifier;
			$hints[ $id ] = $fieldType->description;
		}
		return $hints;
	}

	/**
	 * [hasField description]
	 * 
	 * @param  string  $identifier [description]
	 * @return boolean             [description]
	 */
	public function hasField($identifier)
	{
		foreach ($this->getFieldRelations() as $relation)
		{
			if ($relation->field->fieldType->identifier == $identifier)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the name of the entity type.
	 * 
	 * @return string
	 */
	public function getEntityType()
	{
		return preg_replace('/(?<! )(?<!^)[A-Z]/',' $0', get_class($this));
	}

	/**
	 * Gets the model attributes combined with the custom fields.
	 * 
	 * @return array
	 */
	public function getAllAttributes() // TO DO : Declare this in ActiveRecord ??
	{
		return array_merge($this->attributes, $this->_fieldValues);
	}

	/**
	 * Gets all of the custom fields.
	 * 
	 * @return array
	 */
	public function getFields()
	{
		return $this->_fieldValues;
	}

	/**
	
		Validation

	 */
	
	/**
	 * Declares the validation rules for each field type.
	 */
	public function rules()
	{
		$rules = array();

		foreach ($this->_fieldTypes as $fieldType)
		{
			$id = $fieldType->identifier;
			$rules[] = array($id, 'safe');
			$rules[] = array($id, 'length', 'max' => 45);
		}
		return $rules;
	}

	/**
	
		Events

	 */
	
	protected function afterFind()
	{
		parent::afterFind();

		// Populate the field values from the relation models.
		foreach ($this->getFieldRelations() as $relation)
		{
			$id = $relation->field->fieldType->identifier;
			$this->_fieldValues[ $id ] = $relation->field->field_value;
		}
	}

	protected function afterSave()
	{
		parent::afterSave();
		$this->saveFields();
	}

	/**
	 * Saves all current fields to the database and relates the fields to this entity.
	 */
	private function saveFields()
	{
		if (!$this->isNewRecord)
		{
			// Update each custom field.
			foreach ($this->getFieldRelations() as $relation)
			{
				$field = $relation->field;
				$field->field_value = $this->_fieldValues[ $field->fieldType->identifier ];
				$field->save();
			}
		}

		// Create a new field and relation for each field that does not exist.
		foreach ($this->_fieldTypes as $fieldType)
		{
			if (!$this->hasField($fieldType->identifier))
			{
				$field = new Field;
				$field->field_type_id = $fieldType->field_type_id;
				$field->field_value = $this->_fieldValues[ $fieldType->identifier ];
				$field->save();

				$relation = $this->createFieldRelation();
				$relation->entityID = $this->primaryKey;
				$relation->field_id = $field->field_id;
				$relation->save();
			}
		}	
	}

	/**
		
		Scopes
		
	 */

	/**
	 * Filters criteria by a custom field.
	 * 
	 * @param  string       $id        The identifier of the field type to filter by.
	 * @param  string       $value     The value of the field to filter by.
	 * @param  CDbCriteria  $criteria  The criteria to merge with.
	 * @return CustomEntity            A reference to this.
	 */
	public function filterField($id, $value, $criteria)
	{
		$name = FieldType::model()->parseIdentifier($id);

		$relationTable = lcfirst(get_class($this)) . 'HasFields';
		$pkColumn = strtolower(preg_replace('/\B([A-Z])/', '_$1', get_class($this))) . '_id';

		$newCriteria = new CDbCriteria;
		$newCriteria->with = array(
			$relationTable => array('select' => false),
			$relationTable . '.field.fieldType',
		);
		$newCriteria->group = $this->tableAlias . '.' . $pkColumn;
		$newCriteria->compare('fieldType.field_type_name', $name);

		if (is_array($value))
		{
			$newCriteria->addBetweenCondition('field.field_value', $value[0], $value[1]);
		}
		else
		{
			$newCriteria->compare('field.field_value', $value);
		}

		if (empty($criteria->having))
		{
			$criteria->having = 'count(' . $this->tableAlias . '.' . $pkColumn . ')=1';
		}
		else
		{
			$parts = explode('=', $criteria->having);
			$criteria->having = $parts[0] . '=' . ($parts[1] + 1);
		}

		$criteria->mergeWith($newCriteria, 'OR');

		return $this;
	}
	
	/**
	
		Magic

	 */

	/**
	 * Overrides the getter to retrieve dynamic field data.
	 */
	public function __get($name)
	{
		if (isset($this->_fieldValues[$name]))
		{
			return $this->_fieldValues[$name];
		}
		else
		{
			return parent::__get($name);
		}
	}
	
	/**
	 * Overrides the setter to store dynamic field data.
	 */
	public function __set($name, $val)
	{
		if (isset($this->_fieldValues[$name]))
		{
			if (!is_null($val))
			{
				$this->_fieldValues[$name] = $val;
			}
		}
		else
		{
			parent::__set($name, $val);
		}
	}
}
