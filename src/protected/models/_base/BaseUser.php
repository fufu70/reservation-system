<?php

/**
 * This is the model base class for the table "{{user}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "User".
 *
 * Columns in table "{{user}}" available as properties of the model,
 * followed by relations of table "{{user}}" available as properties of the model.
 *
 * @property integer $user_id
 * @property string $email_address
 * @property string $last_name
 * @property string $first_name
 *
 * @property BranchHasUser[] $branchHasUsers
 * @property Log[] $logs
 * @property Login $login
 * @property Reservation[] $reservations
 * @property SystemHasUser[] $systemHasUsers
 * @property UserHasField[] $userHasFields
 */
abstract class BaseUser extends CustomEntity {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{user}}';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'User|Users', $n);
	}

	public static function representingColumn() {
		return 'email_address';
	}

	public function rules() {
		$rules = array(
			array('email_address, last_name, first_name', 'required'),
			array('email_address, last_name, first_name', 'length', 'max'=>45),
			array('user_id, email_address, last_name, first_name', 'safe', 'on'=>'search'),
		);
		return array_merge($rules, parent::rules());
	}

	public function relations() {
		return array(
			'branchHasUsers' => array(self::HAS_MANY, 'BranchHasUser', 'user_id'),
			'logs' => array(self::HAS_MANY, 'Log', 'user_id'),
			'login' => array(self::HAS_ONE, 'Login', 'user_id'),
			'reservations' => array(self::HAS_MANY, 'Reservation', 'user_id'),
			'systemHasUsers' => array(self::HAS_MANY, 'SystemHasUser', 'user_id'),
			'userHasFields' => array(self::HAS_MANY, 'UserHasField', 'user_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		$labels = array(
			'user_id' => Yii::t('app', 'User'),
			'email_address' => Yii::t('app', 'Email Address'),
			'last_name' => Yii::t('app', 'Last Name'),
			'first_name' => Yii::t('app', 'First Name'),
			'branchHasUsers' => null,
			'logs' => null,
			'login' => null,
			'reservations' => null,
			'systemHasUsers' => null,
			'userHasFields' => null,
		);
		return array_merge($labels, parent::attributeLabels());
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('email_address', $this->email_address, true);
		$criteria->compare('last_name', $this->last_name, true);
		$criteria->compare('first_name', $this->first_name, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}