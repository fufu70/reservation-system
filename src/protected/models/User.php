<?php

Yii::import('application.models._base.BaseUser');

/**
 * The User class is there to represent all of the relations in the database. This
 * includes the users primary source of unique identification, its email address, 
 * and everything associated with it. The users role also defines the actions
 * that he/she may take in the reservation system.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class User extends BaseUser
{
	public $password;
	public $current_user_role;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		$labels = array(
			'current_user_role' => 'User Role',
		);
		return array_merge(parent::attributeLabels(), $labels);
	}

	/**
	 * Declares the attribute input types.
	 */
	public function inputTypes()
	{
		$inputTypes = array(
			'user_id' => 'hidden',
			'first_name' => InputType::model()->getPathToWidget(InputType::TEXT),
			'last_name' => InputType::model()->getPathToWidget(InputType::TEXT),
			'email_address' => InputType::model()->getPathToWidget(InputType::EMAIL_ADDRESS),
			'current_user_role' => InputType::model()->getPathToWidget(InputType::USER_ROLE),
		);
		return array_merge($inputTypes, parent::inputTypes());
	}

	public function getAllAttributes()
	{
		$attributes = array(
			'current_user_role' => $this->current_user_role
		);
		return array_merge($attributes, parent::getAllAttributes());
	}

	public function createFieldRelation()
	{
		return new UserHasField;
	}

	public function getFieldRelations()
	{
		return $this->userHasFields;
	}

	/**
	 * Gets all branches that the user has access to. If the system_id session
	 * variable is set, then only branches of that system are returned.
	 * 
	 * @return array<Branch>
	 */
	public function getBranches()
	{
		if (!empty(Yii::app()->user->system_id))
		{
			return Branch::model()
				->system(Yii::app()->user->system_id)
				->systemUser($this->user_id)
				->findAll();
		}
		else
		{
			return Branch::model()
				->systemUser($this->user_id)
				->findAll();
		}
	}

	/**
	 * Gets the user role related to the specified branch.
	 *
	 * @param  int 		$branchID 	The ID of the branch.
	 * @return string
	 */
	public function getRoleInBranch($branchID)
	{
		foreach ($this->branchHasUsers as $relation)
		{
			if ($relation->branch_id == $branchID)
			{
				return $relation->userRole->user_role_name;
			}
		}

		return null;
	}

	/**
	 * Gets the combined first and last name of the user.
	 * 
	 * @return string
	 */
	public function getFullName()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	/**
	
		Validation

	 */
	
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = array(
			array('current_user_role', 'safe'),
			array('email_address', 'required'),
			array('email_address', 'email'),
			array('email_address', 'unique'),
		);
		return array_merge(parent::rules(), $rules);
	}

	/**
	
		Events

	 */
	
	protected function afterFind()
	{
		parent::afterFind();

		if (Yii::app()->user->isBranchSelected())
		{
			$this->current_user_role = $this->getRoleInBranch(Yii::app()->user->branch->branch_id);
		}
	}
	
	protected function afterSave()
	{
		parent::afterSave();

		if (Yii::app()->user->isBranchSelected())
		{
			$this->updateUserRole();
		}

		if ($this->isNewRecord && isset($this->password))
		{
			$login = new Login;
			$login->user_id = $this->user_id;
			$login->password = Login::createHash($this->password);
			$login->save();
		}
	}

	/**
	 * Updates the user role for the current branch.
	 */
	private function updateUserRole()
	{
		$relation = BranchHasUser::model()
			->currentBranch()
			->user($this->user_id)
			->find();

		if (isset($relation))
		{
			$relation->user_role_id = UserRole::model()->name($this->current_user_role)->find()->user_role_id;
			$relation->save();
		}
		else
		{
			$relation = new BranchHasUser;
			$relation->branch_id = Yii::app()->user->branch->branch_id;
			$relation->user_id = $this->user_id;
			$relation->user_role_id = UserRole::model()->name($this->current_user_role)->find()->user_role_id;
			$relation->save();
		}
	}

	/**
		
		Scopes
		                                                               
	 */
	
	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return User    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}
	
	/**
	 * Filters criteria by email address.
	 * 
	 * @param  string $emailAddress 	The email address to filter by.
	 * @return User                 	A reference to this.
	 */
	public function emailAddress($emailAddress)
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 'email_address = :emailAddress',
            'params' => array(':emailAddress' => $emailAddress),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by system.
	 * 
	 * @param  int 	$systemID 	The ID of the system to filter by.
	 * @return User             A reference to this.
	 */
	public function system($systemID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'with' => array('systemHasUsers' => array('select' => false)),
	        'condition' => 'systemHasUsers.system_id = :systemID',
            'params' => array(':systemID' => $systemID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 	$branchID 	The ID of the branch to filter by.
	 * @return User             A reference to this.
	 */
	public function branch($branchID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'with' => array('branchHasUsers' => array('select' => false)),
	        'condition' => 'branchHasUsers.branch_id = :branchID',
            'params' => array(':branchID' => $branchID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by users that have not been deleted, in relation to the branch.
	 * 
	 * @return User             A reference to this.
	 */
	public function unarchived()
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'with' => array('branchHasUsers' => array('select' => false)),
	        'condition' => 'branchHasUsers.archived_date IS NULL',
	    ));
	    return $this;
	}
}
