<?php

Yii::import('application.models._base.BaseFieldType');

/**
 * The FieldType class is there to represent all of the rows in the FieldType
 * table. The FieldType referes to several different points in the System, a header
 * for columns, a definition for what input the columns should have when creating them, 
 * and the name of the column for loggin purposes.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class FieldType extends BaseFieldType
{
	public $entity_type_name;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		$labels = array(
			'field_type_name' => 'Field Name',
			'input_type_id' => 'Input Type',
			'description' => 'Field Description',
		);
		return array_merge(parent::attributeLabels(), $labels);
	}

	/**
	 * Declares the attribute input types.
	 */
	public function inputTypes()
	{
		return array(
			'field_type_id' => 'hidden',
			'entity_type_name' => 'hidden',
			'field_type_name' => InputType::model()->getPathToWidget(InputType::TEXT),
			'input_type_id' => InputType::model()->getPathToWidget(InputType::INPUT_TYPE),
			'description' => InputType::model()->getPathToWidget(InputType::TEXT_AREA),
		);
	}

	/**
	 * Declares the attribute hints.
	 */
	public function hints()
	{
		return array();
	}

	public function getIdentifier()
    {
    	return str_replace(' ', '_', $this->field_type_name);
    }

    public function parseIdentifier($name)
    {
    	return str_replace('_', ' ', $name);
    }

    /**
	
		Validation

	 */
	
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = array(
			array('field_type_id', 'required', 'on' => 'update'),
			array('entity_type_name', 'safe'),
			array('field_type_name', 'uniqueName'),

			// TO DO : Check that the field type name is not the same as the default entity fields.
			// TO DO : Check that the field type name has no illegal characters (underscores).
		);
		return array_merge(parent::rules(), $rules);
	}

	/**
	 * Validator that checks if the field type name is unique to the entity within the current branch.
	 */
	public function uniqueName()
	{
		$exists = FieldType::model()
			->entityType($this->entity_type_name)
			->name($this->field_type_name)
			->currentBranch()
			->exists();

		if ($exists)
		{
            $this->addError('field_type_name', 'This field name already exists.');
        }
	}

    /**
	
		Events

	 */
	
	protected function beforeValidate()
	{
		if (isset($this->entity_type_name))
		{
			$this->entity_type_id = EntityType::model()->name($this->entity_type_name)->find()->entity_type_id;
		}

		return parent::beforeValidate();
	}

	protected function afterSave()
	{
		parent::afterSave();

		if ($this->isNewRecord)
		{
			$relation = new BranchHasFieldType;
			$relation->branch_id = Yii::app()->user->branch->branch_id;
			$relation->field_type_id = $this->field_type_id;
			$relation->save();
		}
	}

	/**
	   
	   Scopes
		                                                               
	 */
	
	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return Reservation    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}
	
	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 		$branchID 		The ID of the branch to filter by.
	 * @return FieldType            	A reference to this.
	 */
	public function branch($branchID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('branchHasFieldTypes' => array('select' => false)),
	        'condition' => 'branchHasFieldTypes.branch_id = :branchID',
            'params' => array(':branchID' => $branchID),
	    ));
	    return $this;
	}
	
	/**
	 * Filters criteria by field type name.
	 * 
	 * @param  string 	$name 			The name of the field type to filter by.
	 * @return FieldType            	A reference to this.
	 */
	public function name($name)
	{
		$this->getDbCriteria()->mergeWith(array(
	        'condition' => 'field_type_name = :name',
            'params' => array(':name' => $name),
	    ));
	    return $this;
	}
	
	/**
	 * Filters criteria by entity type.
	 * 
	 * @param  string 	$entityType 	The name of the entity type to filter by.
	 * @return FieldType            	A reference to this.
	 */
	public function entityType($entityType)
	{
		$this->getDbCriteria()->mergeWith(array(
	    	'with' => 'entityType',
	        'condition' => 'entityType.entity_type_name = :entityType',
            'params' => array(':entityType' => $entityType),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by input type.
	 * 
	 * @param  string 	$inputType 		The name of the input type to filter by.
	 * @return FieldType            	A reference to this.
	 */
	public function inputType($inputType)
	{
		$this->getDbCriteria()->mergeWith(array(
			'with' => 'inputType',
			'condition' => 'inputType.input_type_name = :inputType',
			'params' => array(':inputType' => $inputType),
		));
		return $this;
	}
}
