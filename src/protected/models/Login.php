<?php

Yii::import('application.models._base.BaseLogin');

/**
 * The Login class provides functions for creating and updating user login information which is 
 * stored as pairs of user IDs and hashed passwords.
 *
 * @author   John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class Login extends BaseLogin
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 *  Validates a given password against the hashed password found
	 *  in the Login table for a specific user ID.
	 * 
	 * 	@param  string   $userID    The ID of the user to validate for.
	 *  @param  string   $password 	The users atempted login password.
	 *  @return boolean             Whether the password is a match.
	 */
	public static function authenticate($userID, $password)
	{
        $login = Login::model()->find(array(
			'select' => 'password', 
			'condition' => 'user_id=:id', 
			'params' => array(':id' => $userID)
		));
		return (isset($login)) ? Login::validatePassword($password, $login->password) : false;
	}

	/**
	 * Creates a hash for a given password.
	 * 
	 * @param  string   $password   The password to be hashed.
	 * @return string               The hashed password.
	 */
	public static function createHash($password)
	{
		return Yii::app()->hash->create_hash($password);
	}

	/**
	 * Compares a given password to a hashed login password.
	 * 
	 * @param  string 	$password     	The given password.
	 * @param  string 	$hashPassword 	The hashed login password.
	 * @return boolean              	Whether the password is a match.
	 */
	public static function validatePassword($password, $hashPassword)
    {
        return Yii::app()->hash->validate_password($password, $hashPassword);
    }
}
