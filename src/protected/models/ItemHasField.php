<?php

Yii::import('application.models._base.BaseItemHasField');
Yii::import('application.models._interfaces.FieldRelation');

class ItemHasField extends BaseItemHasField implements FieldRelation
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getCustomEntity()
	{
		return new Item;
	}

	public function getEntityID()
	{
		return $this->item_id;
	}

	public function setEntityID($id)
	{
		$this->item_id = $id;
	}
}