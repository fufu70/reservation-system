<?php

Yii::import('application.models._base.BaseReservationHasItem');

class ReservationHasItem extends BaseReservationHasItem
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
