<?php

Yii::import('application.models._base.BaseSetting');

/**
 * The Setting class exists to have information regarding the all of the rows in the Settings
 * table. In Xeres the settings represent the changes in what is displayed to the user. These
 * settings can refer to the name of the item that is displayed to the background.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class Setting extends BaseSetting
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
		
		Scopes
		                                                               
	 */
	
	/**
	 * Declares all non-parameterized scopes.
	 * 
	 * @return array
	 */
	public function scopes()
	{
		return array(
			'activated' => array(
				'condition' => 'setting_activated = 1',
			),
			'deactivated' => array(
				'condition' => 'setting_activated = 0',
			),
		);
	}
	
	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return Setting    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}
	
	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 		$branchID 	The ID of the branch to filter by.
	 * @return Setting              A reference to this.
	 */
	public function branch($branchID)
	{
	    $this->getDbCriteria()->mergeWith(array(
			'condition' => 't.branch_id = :branchID',
			'params' => array(':branchID' => $branchID),
		));
	    return $this;
    }

	/**
	 * Filters criteria by setting type name.
	 * 
	 * @param  string 	$name 	The name of the setting type to filter by.
	 * @return Setting          A reference to this.
	 */
	public function type($name)
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'with' => array('settingType' => array('select' => false)),
	        'condition' => 'settingType.setting_type_name = :name',
            'params' => array(':name' => $name),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by setting attribute.
	 * 
	 * @param  int 		$attribute 	The setting attribute to filter by.
	 * @return Setting              A reference to this.
	 */
	public function attribute($attribute)
	{
	    $this->getDbCriteria()->mergeWith(array(
			'condition' => 't.setting_attribute = :attribute',
			'params' => array(':attribute' => $attribute),
		));
	    return $this;
    }
}
