<?php

Yii::import('application.models._base.BaseInputType');

/**
 * InputType represents all of the input type rows in the table.
 * The input type states what the design will be for the input box. The input
 * could be a date picker, a time picker, checkbox, or a regular string.
 *
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v2.0.0
 */
class InputType extends BaseInputType
{
	const PATH_TO_INPUT_WIDGETS = 'application.components._input.';
	
	// Basic Input Types
	const TEXT = 'Text';
	const TEXT_AREA = 'Text Area';
	const CHECKBOX = 'Checkbox';
	const DATE_TIME = 'Date & Time';
	const DATE = 'Date';
	const TIME = 'Time';
	const TIME_ZONE = 'Time Zone';
	const EMAIL_ADDRESS = 'Email Address';
	const PHONE_NUMBER = 'Phone Number';
	const DOLLAR = 'Dollar';
	const COLOR = 'Color';

	// Table Input Types
	const ACCESS = 'Access';
	const BRANCH = 'Branch';
	const ENTITY_TYPE = 'Entity Type';
	const FIELD = 'Field';
	const FIELD_TYPE = 'Field Type';
	const INPUT_TYPE = 'Input Type';
	const ITEM = 'Item';
	const ITEM_STATUS = 'Item Status';
	const ITEM_TYPE = 'Item Type';
	const LOCATION = 'Location';
	const LOG = 'Log';
	const MODULE = 'Module';
	const RESERVATION = 'Reservation';
	const RESERVATION_STATUS = 'Reservation Status';
	const SETTING = 'Setting';
	const SETTING_TYPE = 'Setting Type';
	const SYSTEM = 'System';
	const USER = 'User';
	const USER_ROLE = 'User Role';

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Gets the path of alias to an input's widget.
	 *
	 * @param  string 	$name 	The name of the input type. If not specified, the object's 
	 *                        	input type name is used.
	 * @return string  			The path to the widget.
	 */
	public function getPathToWidget($name = null)
	{
		$name = isset($name) ? $name : $this->input_type_name;
		$search = array(' ', '&');

		return self::PATH_TO_INPUT_WIDGETS . str_replace($search, '', $name) . 'Widget';
	}

	/**
	   
	   Scopes
																	   
	 */
	
	/**
	 * Declares all non-parameterized scopes.
	 * 
	 * @return array
	 */
	public function scopes()
	{
		return array(
			'visible' => array(
				'condition' => 'visible = 1',
			),
			'invisible' => array(
				'condition' => 'visible = 0',
			),
		);
	}
	
	/**
	 * Filters criteria by input type name.
	 * 
	 * @param  string 	$name	The name of the input type to filter by.
	 * @return InputType   		A reference to this.
	 */
	public function name($name)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'input_type_name = :name',
			'params' => array(':name' => $name),
		));
		return $this;
	}
}
