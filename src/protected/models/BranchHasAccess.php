<?php

Yii::import('application.models._base.BaseBranchHasAccess');

class BranchHasAccess extends BaseBranchHasAccess
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
