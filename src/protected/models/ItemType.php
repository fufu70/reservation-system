<?php

Yii::import('application.models._base.BaseItemType');

/**
 * The ItemType class is there to represent all of the information regarding the rows in the
 * Item Type table.
 * 
 * @author   Christian Micklisch <cmicklis@stetson.edu>, John Salis <jsalis@stetson.edu>
 * @since 	 v1.0.0
 */
class ItemType extends BaseItemType
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    /**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = array(
			array('item_type_name', 'unique', 'criteria' => self::model()->currentBranch()->getDbCriteria()),
		);
		return array_merge(parent::rules(), $rules);
	}

    /**
	 * Declares the attribute labels.
	 */
	public function attributeLabels()
	{
		$itemName = Setting::model()->type(SettingType::ITEM_NAME)->currentBranch()->find()->setting_value;

		$labels = array(
			'item_type_name' => $itemName . ' Type',
		);
		return array_merge(parent::attributeLabels(), $labels);
	}

	/**
	 * Declares the attribute input types.
	 */
	public function inputTypes()
	{
		$inputTypes = array(
			'item_type_id' => 'hidden',
			'item_type_name' => InputType::model()->getPathToWidget(InputType::TEXT),
		);
		return array_merge($inputTypes, parent::inputTypes());
	}

    public function createFieldRelation()
	{
		return new ItemTypeHasField;
	}

	public function getFieldRelations()
	{
		return $this->itemTypeHasFields;
	}

	/**
	
		Events

	 */
	
	protected function beforeValidate()
	{
		$this->branch_id = Yii::app()->user->branch->branch_id;

		return parent::beforeValidate();
	}

	/**
		
		Scopes
		                                                               
 	 */
 	
 	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return ItemType    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}
	
	/**
	 * Filters criteria by branch.
	 * 
	 * @param  int 		$branchID 	The ID of the branch to filter by.
	 * @return ItemType             A reference to this.
	 */
	public function branch($branchID)
	{
	    $this->getDbCriteria()->mergeWith(array(
			'condition' => 't.branch_id = :branchID',
			'params' => array(':branchID' => $branchID),
		));
	    return $this;
    }
 
 	/**
	 * Filters criteria by item type name.
	 * 
	 * @param  string	$name 	The name of the item type to filter by.
	 * @return ItemType      	A reference to this.
	 */
	public function name($name)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.item_type_name = :name',
			'params' => array(':name' => $name),
		));
		return $this;
	}

	/**
	 * Filters criteria by item types that have not been deleted.
	 * 
	 * @return ItemType         A reference to this.
	 */
	public function unarchived()
	{
	    $this->getDbCriteria()->mergeWith(array(
	        'condition' => 't.archived_date IS NULL',
	    ));
	    return $this;
	}
}
