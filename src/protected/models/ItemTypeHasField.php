<?php

Yii::import('application.models._base.BaseItemTypeHasField');
Yii::import('application.models._interfaces.FieldRelation');

class ItemTypeHasField extends BaseItemTypeHasField implements FieldRelation
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getCustomEntity()
	{
		return new ItemType;
	}

	public function getEntityID()
	{
		return $this->item_type_id;
	}

	public function setEntityID($id)
	{
		$this->item_type_id = $id;
	}
}