<?php

Yii::import('application.models._base.BaseBranchHasUser');

class BranchHasUser extends BaseBranchHasUser
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
		
		Scopes

	 */
	
	public function scopes()
	{
		return array(
			'disableDefaultScope'
		);
	}
	
	/**
	 * Filters criteria by the current branch.
	 * 
	 * @return BranchHasUser    A reference to this.
	 */
	public function currentBranch()
	{
		return $this->branch(Yii::app()->user->branch->branch_id);
	}

	/**
	 * 	Filters criteria by branch.
	 *
	 * 	@param  int 	branchID 	The ID of the branch to filter by.
	 * 	@return BranchHasUser 		A reference to this.
	 */
	public function branch($branchID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.branch_id = :branchID',
			'params' => array(':branchID' => $branchID)
		));
		return $this;
	}

	/**
	 * 	Filters criteria by user.
	 *
	 * 	@param  int 	userID 		The ID of the user to filter by.
	 * 	@return BranchHasUser  		A reference to this.
	 */
	public function user($userID)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.user_id = :userID',
			'params' => array(':userID' => $userID)
		));
		return $this;
	}
}
