<?php

Yii::import('application.models._base.BaseField');

class Field extends BaseField
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	
		Validation
		                                                               
	 */

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		$rules = array(
			array('field_value', 'default', 'value' => ''),
		);
		return array_merge(parent::rules(), $rules);
	}

	/**
	
		Scopes
		                                                               
	 */
	
	/**
	 * Filters criteria by field type.
	 * 
	 * @param  int 	$fieldTypeID 	The ID of the field type to filter by.
	 * @return Field            	A reference to this.
	 */
	public function type($fieldTypeID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => 'fieldType',
	        'condition' => 'fieldType.field_type_id = :fieldTypeID',
            'params' => array(':fieldTypeID' => $fieldTypeID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by item.
	 * 
	 * @param  int 		$itemID 	The ID of the item to filter by.
	 * @return Field                A reference to this.
	 */
	public function item($itemID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('itemHasFields' => array('select' => false)),
	        'condition' => 'itemHasFields.item_id = :itemID',
            'params' => array(':itemID' => $itemID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by item status.
	 * 
	 * @param  int 		$itemStatusID 	The ID of the item status to filter by.
	 * @return Field                	A reference to this.
	 */
	public function itemStatus($itemStatusID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('itemStatusHasFields' => array('select' => false)),
	        'condition' => 'itemStatusHasFields.item_status_id = :itemStatusID',
            'params' => array(':itemStatusID' => $itemStatusID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by item type.
	 * 
	 * @param  int 		$itemTypeID 	The ID of the item type to filter by.
	 * @return Field                	A reference to this.
	 */
	public function itemType($itemTypeID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('itemTypeHasFields' => array('select' => false)),
	        'condition' => 'itemTypeHasFields.item_type_id = :itemTypeID',
            'params' => array(':itemTypeID' => $itemTypeID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by location.
	 * 
	 * @param  int 		$locationID 	The ID of the location to filter by.
	 * @return Field                	A reference to this.
	 */
	public function location($locationID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('locationHasFields' => array('select' => false)),
	        'condition' => 'locationHasFields.location_id = :locationID',
            'params' => array(':locationID' => $locationID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by reservation.
	 * 
	 * @param  int 		$reservationID 	The ID of the reservation to filter by.
	 * @return Field                	A reference to this.
	 */
	public function reservation($reservationID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('reservationHasFields' => array('select' => false)),
	        'condition' => 'reservationHasFields.reservation_id = :reservationID',
            'params' => array(':reservationID' => $reservationID),
	    ));
	    return $this;
	}

	/**
	 * Filters criteria by user.
	 * 
	 * @param  int 		$userID 		The ID of the user to filter by.
	 * @return Field                	A reference to this.
	 */
	public function user($userID)
	{
	    $this->getDbCriteria()->mergeWith(array(
	    	'with' => array('userHasFields' => array('select' => false)),
	        'condition' => 'userHasFields.user_id = :userID',
            'params' => array(':userID' => $userID),
	    ));
	    return $this;
	}
}
