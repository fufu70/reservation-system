jQuery(function($) {

	$('#FieldType_field_type_id').change(function() {
		var self = $(this);
		var id = self.find('option:selected').val();
		$.ajax({
			type: 'GET',
			url: '/fieldType/update',
			data: { 'id': id },
			dataType: 'html',
			success: function(data) {
				data = JSON.parse(data);
				var form = self.closest('form');
				form.find('#FieldType_field_type_name').val(data.field_type_name);
				form.find('#FieldType_description').val(data.description);
			},
		});
	});
});
