jQuery(function($) {

	$('#FieldType_input_type_id').change(function() {
		var inputType = $(this).find('option:selected').text().trim();
		$('#field_input_preview').html();
		$.ajax({
			type: 'GET',
			url: '/inputType/fetchInput',
			data: { 'inputType': inputType },
			dataType: 'html',
			cache: false,
			success: function(data) {
				$('#field_input_preview').html(data);
				initBehavior($('#field_input_preview'));
			},
		});
	});

	$('#FieldType_field_type_name').keyup(function() {
		if ($(this).val() !== '') {
			$('#field_name_preview').html($(this).val());
		} else {
			$('#field_name_preview').html('Field Name');
		}
	});

	$('#FieldType_description').keyup(function() {
		$('#field_description_preview').html($(this).val());
	});
});
