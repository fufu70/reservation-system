/**
 * Filters incoming ajax data so that scripts are not included more than once. This is a solution
 * to a known Yii issue with rendering partial views through ajax requests.
 */

$.ajaxSetup({

	global: true,
	dataFilter: function(data, type) {

		// only 'text' and 'html' dataType should be filtered
		if (type && type != "html" && type != "text")
		{
			return data;
		}

		// copy-paste the following back in after [src] if you also want stylesheets blocked too: ,link[rel="stylesheet"]
		var selector = 'script[src]';

		// get loaded scripts from DOM the first time we execute.
		if (!$._loadedScripts)
		{
			$._loadedScripts = {};
			$._dataHolder = $(document.createElement('div'));

			var loadedScripts = $(document).find(selector);

			// fetching scripts from the DOM
			for (var i = 0, len = loadedScripts.length; i < len; i++)
			{
				$._loadedScripts[loadedScripts[i].src] = 1;
			}
		}

		// $._dataHolder.html(data) does not work
		$._dataHolder[0].innerHTML = data;

		// iterate over new scripts and remove if source is already in DOM:
		var incomingScripts = $($._dataHolder).find(selector);

		for (var i = 0, len = incomingScripts.length; i < len; i++)
		{
			if ($._loadedScripts[incomingScripts[i].src])
			{
				$(incomingScripts[i]).remove();
			}
			else
			{
				$._loadedScripts[incomingScripts[i].src] = 1;
			}
		}

		return $._dataHolder[0].innerHTML;
	}
});
