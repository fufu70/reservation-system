jQuery(function($) {
	
	// Equipment selections will NOT submit if disabled, thus when the form is submitted, 
	// this function enables all the equipment options hidden by the filter
	$('.form-signin').submit(function(event) {
		$("#filter_equipment*").val('All').trigger("change").trigger("chosen:updated");
	});


	var stepComplete = false;

	$('.wizard').on('actionclicked.fu.wizard', function (e, data) {

		if (stepComplete || data.direction == 'previous') {
			stepComplete = false;
			return;
		}

		var wizard = $(this);
		var stepPane = wizard.find('.step-pane.active');
		var form = wizard.closest('form');
		var settings = form.data('settings');
		settings.submitting = true;

		$.fn.yiiactiveform.validate(form, function(messages) {

			var hasError = false;

			if (!$.isEmptyObject(messages)) {

				$.each(settings.attributes, function() {
					// If the attribute is in the current step.
					if (stepPane.find('#' + this.inputID).length > 0) {
						// If the attribute has an error message.
						if (messages.hasOwnProperty(this.id)) {
							hasError = true;
						}
						$.fn.yiiactiveform.updateInput(this, messages, form);
					}
				});

				settings.submitting = false;
			}

			if (!hasError) {
				stepComplete = true;
				wizard.wizard('next');
			}
		});
		e.preventDefault();
	});

	$('.wizard').on('finished.fu.wizard', function (e, data) {
		$(this).closest('form').submit();
	});

	$('.wizard').on('changed.fu.wizard', function (e, data) {
		setTimeout(function() {
			$('.hint').hide();
		}, 1);
	});
});
