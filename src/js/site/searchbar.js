jQuery(function($) {
	$('#search-bar').hide();
	$('#search-bar').animate({
		'width' : '0px'
	});
	$('#search-button').click(function() {
		$('#search-bar').show("slow");
		$('#search-bar').animate({
			'width' : '200px'
		});
		$('#search-button').delay(400).hide(1000);
		$('#search-bar').focus();
	});
	$("#search-bar").focusout(function() {
		$('#search-bar').animate({
			'width' : '0px'
		});
		$('#search-bar').hide("slow");
		$('#search-button').show("slow");
	});
});