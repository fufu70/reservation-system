
/** 
 * GENERAL BEHAVIOR ACTIVATION
 */
function initBehavior(root) {

	root.find('input, textarea, select').not(':input[type=button], :input[type=submit]').addClass('form-control');

	root.find('.btn-group').button();

	root.find('form').submit(function() {
		$(this).find('button[type="submit"]').prop('disabled', true).html('Loading...');
	});

	/**
	 * HINT
	 */
	root.find('.hint').hide();
	root.find('input, textarea, select, button').on('focus', function() {
		$('.hint').hide();
		$(this).closest('.form-group').find('.hint').show();
	});
	root.find('.switcher').on('focus.bootstrapSwitch', function(event, state) {
		$('.hint').hide();
		$(this).closest('.form-group').find('.hint').show();
	});

	/**
	 * CHOSEN
	 */
	root.find('.chosen').chosen({
		placeholder_text_single: ' ',
		placeholder_text_multiple: ' ',
		disable_search_threshold: 6,
		search_contains: true,
		enable_split_word_search: true,
		display_disabled_options: false,
		allow_single_deselect: false,
		width: '100%',
	});

	$('.close-panel').click(function() {
		$(this).closest('.panel').hide(300, function() {
			$(this).remove();
		});
	});
}

/**
 * The openSidebar method goes and opens the sidebar by simply adding the sidebar-open
 * class to the content-container and the sidebar transition class. This is done only if
 * the menu the button does not have the side-bar clicked method. The button gets both a 
 * sidebar-clicked class associated with it to notify that the button has opened the toggle 
 * as well as a click-in-process class. The click-in-process class is then removed after a 
 * millisecond. 
 * 
 * This class is added to halt the process of the sidebar closing since the user is both
 * clicking in the element, and the body of the content. When the user clicks in the body
 * of the content (away from the sidebar) he/she should be allowed to also close the sidebar.
 */
function openSidebar() {
	
	if (!$('#menu-toggle').hasClass('sidebar-clicked')) {

		$('.pusher-overlay').fadeIn(400);
		$('#menu-toggle').addClass('sidebar-clicked');
		$('#menu-toggle').addClass('sidebar-click-in-process');

		$('.content-container').addClass('sidebar-open');
		$('.content-container').addClass('sidebar-transition');

		setTimeout(function() {
			$('#menu-toggle').removeClass('sidebar-click-in-process');
		}, 1);

		$('.pusher').click(function() {
			if ($('.content-container').hasClass('sidebar-open') && !$('#menu-toggle').hasClass('sidebar-click-in-process')) {
				closeSidebar();
			}
		});

		$('.pusher-overlay').click(function() {
			if ($('.content-container').hasClass('sidebar-open') && !$('#menu-toggle').hasClass('sidebar-click-in-process')) {
				closeSidebar();
			}
		});
	}
}

/**
 * The closeSidebar method goes and removes the sidebar-open class as well as the transition
 * class, but only after the transition has finished (plus a couple extra milliseconds). We 
 * then remove the click function associated with the pusher class and remove the sidebar-clicked
 * class with the toggle button.
 */
function closeSidebar() {

	$('.pusher-overlay').fadeOut(400);

	$('.content-container').removeClass('sidebar-open');
	$('.sidebar-transition').css('transition-duration');

	setTimeout(function() {
		$('.content-container').removeClass('sidebar-transition');
	}, 400);


	$('.pusher').unbind();
	$('.pusher-overlay').unbind();
	$('#menu-toggle').removeClass('sidebar-clicked');
}

jQuery(function($) {

	initBehavior($(':root'));

	$('[data-toggle="tooltip"]').tooltip({
		container: 'body',
		delay: { 'show': 500, 'hide': 0 }
	});
	$('[data-toggle="popover"]').popover();
	$('.alert-success').delay(5000).fadeOut(1000);
	$('.footable').footable({
		breakpoints: {
			phone: 500,
			tablet: 650
		}
	});

	$('#menu-toggle').click(function(){
		openSidebar();
	});

	$('#close-menu').click(function(){
		closeSidebar();
	});

	$('.pusher-overlay').hide();
});
