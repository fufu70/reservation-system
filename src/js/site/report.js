
var addCriteria;

jQuery(function ($) {

	var counters = {};
	var defaultLimit = 1;

	addCriteria = function (html) {

		var content = $.parseHTML(html, document, true);
		var name = $(content).find('.criteria-label').text();

		$(content).hide();
		$('#search_form').prepend(content);

		$(content).find('.close-panel').on('click', function () {

			counters[ name ] --;

			$('#search_criteria a').filter(function () {
				return this.innerHTML == name;
			}).removeClass('disabled').css('cursor', 'pointer');
		});

		processDynamicLabels();
		$(content).show(300);
		initBehavior($(content));
	};

	$('#search_criteria a').on('click', function () {

		var name = $(this).text();
		var limit = limits[ name ] || defaultLimit;

		if (counters[ name ] === undefined) {
			counters[ name ] = 0;
		}

		if (counters[ name ] >= limit) {
			return false;
		}

		counters[ name ] ++;

		if (counters[ name ] >= limit) {
			$(this).addClass('disabled').css('cursor', 'not-allowed');
		}
	});
});
